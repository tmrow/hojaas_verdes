<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'hojas_data');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'lP;nRUc9kMJ-mM}v>;{bY60vH5=4UP$3QF1cgVhPs$(<N7 v=SRY(3}#g>s2s{3C');
define('SECURE_AUTH_KEY', 'vV/@-[?cn{}%Vvvb/tYXpYGRDWF*iszgc({K:U>G-wuABzJU;]X=OVhuuXa[j;p7');
define('LOGGED_IN_KEY', '?)&s]P:( `u.jhAZ&<@7#ieKhH[|}n,hkg$+Bt~JAS<h!D;!6ufatcX=(6:I4>%l');
define('NONCE_KEY', '8%)xiB%F9)dz<Gvjhovn?7`Aj=J3_nitV]Mx}qYLs&-j1[KK36^]8!+7&:Pi2ncU');
define('AUTH_SALT', 'rPyD^hI)lA`O)t2[ysgt#{N!f5?} <Av2Cx-iwWYc]!_38lmx#YpAlmbLPV9LyQI');
define('SECURE_AUTH_SALT', 'k0(p2bSWy}8?61H.U5a*f}{wuKHd9a)k{>I.DN_-z/!:zAFS0av6mHer:GDp^,t6');
define('LOGGED_IN_SALT', 'GP&Dfb1QON?lr0=!UD.bnsYxHSz6n/L0<ol@MoG}!%mBc}(]0s`YTAp|rmVx~j>8');
define('NONCE_SALT', 's$VwC|3w~g+7#R#YgcF3zxGm;qC-5Ne<[i:bQ:/!S51 pZV*!;f@ogtb_jk~{D**');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'hv_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

