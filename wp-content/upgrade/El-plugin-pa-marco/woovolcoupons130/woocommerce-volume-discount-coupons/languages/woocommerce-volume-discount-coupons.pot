# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid   ""
msgstr  "Project-Id-Version: PACKAGE VERSION\n"
        "Report-Msgid-Bugs-To: \n"
        "POT-Creation-Date: 2015-02-20 19:35+0100\n"
        "PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
        "Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
        "Language-Team: LANGUAGE <LL@li.org>\n"
        "Language: \n"
        "MIME-Version: 1.0\n"
        "Content-Type: text/plain; charset=CHARSET\n"
        "Content-Transfer-Encoding: 8bit\n"

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:442
#, php-format
msgid   " in %s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:375
#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:385
#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:424
#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:434
#, php-format
msgid   " or %s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:413
#, php-format
msgid   "%s%s cart discount"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:408
#, php-format
msgid   "%s%s discount"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:404
#, php-format
msgid   "%s%s discount in %s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:399
#, php-format
msgid   "%s%s discount on %s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:401
#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:406
#, php-format
msgid   "%s%s discount on selected products"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:292
msgid   ", "
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons.php:103
msgid   "<em>WooCommerce Volume Discount Coupons</em> needs the <a href="
        "\"http://www.woothemes.com/woocommerce/\" target=\"_blank"
        "\">WooCommerce</a> plugin. Please install and activate it."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:543
msgid   "<strong>Auto-apply Display Options</strong>"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:510
msgid   "<strong>Display Options</strong>"
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:69
msgid   "Access denied."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:40
msgid   "Anywhere"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:489
msgid   "Apply automatically"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:490
msgid   "Apply this coupon automatically when valid based on these conditions."
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:451
#, php-format
msgid   "Buy %1$d %2$s and get a %3$s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:463
#, php-format
msgid   "Buy %1$d %2$s and get a <span class=\"coupon discount\">%3$s</span> "
        "using the coupon code <span class=\"coupon code\">%4$s</span>"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:454
#, php-format
msgid   "Buy %1$d or more %2$s and get a %3$s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:466
#, php-format
msgid   "Buy %1$d or more %2$s and get a <span class=\"coupon discount\">"
        "%3$s</span> using the coupon code <span class=\"coupon code\">%4$s</"
        "span>"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:449
#, php-format
msgid   "Buy %1$d to %2$d %3$s and get a %4$s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:461
#, php-format
msgid   "Buy %1$d to %2$d %3$s and get a <span class=\"coupon discount\">"
        "%4$s</span> using the coupon code <span class=\"coupon code\">%5$s</"
        "span>"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:456
#, php-format
msgid   "Buy up to %1$d %2$s and get a %3$s"
msgstr  ""

#: lib/views/class-woocommerce-volume-discount-coupons-shortcodes.php:468
#, php-format
msgid   "Buy up to %1$d %2$s and get a <span class=\"coupon discount\">%3$s</"
        "span> using the coupon code <span class=\"coupon code\">%4$s</span>"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:554
msgid   "Description"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:561
msgid   "Display the <em>volume discount info</em> when the coupon is "
        "automatically applied."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:555
msgid   "Display the coupon description when the coupon is automatically "
        "applied."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:409
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:424
msgid   "For a customer to be allowed to apply this coupon, at least one of "
        "the chosen products must be in the cart and its quantity must be "
        "within the given minimum and maximum."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:502
msgid   "For product <strong>variations</strong>, if a variable product is "
        "chosen (i.e. the parent to its variations), the quantity in the cart "
        "used to check the minimum or maximum is the combined total of all "
        "variations in the cart."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:512
msgid   "For the products indicated and products in the selected categories, "
        "the product displayed can be enhanced with information based on the "
        "coupon's description and volume discount."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:504
msgid   "If a product variation is chosen (i.e. a product variation derived "
        "by attribute from its parent product), the quantity check is made "
        "for that variation only, independent of other variations in the cart."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:506
msgid   "If both products and categories are indicated, one of the specified "
        "products or a product that belongs to one of the categories must "
        "meet the quantity restrictions."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:500
msgid   "If no product or category is set, the minimum and maximum (or both) "
        "conditions apply to the sum of quantities in the cart."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:549
msgid   "If not empty, display a message when the coupon is automatically "
        "applied."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:448
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:465
msgid   "If one or more categories are chosen, the coupon is valid if at "
        "least one product in the selected categories is in the cart and its "
        "quantity is within the given minimum and maximum."
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:110
msgid   "Inline styles"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:482
msgid   "Input the maximum quantity allowed in the cart. The condition is "
        "met, if for any of the chosen products, or any product in the "
        "selected categories, the quantity in the cart does not exceed the "
        "maximum. If no products or categories are specified, the restriction "
        "applies to the sum of quantities in the cart."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:473
msgid   "Input the minimum quantity that must be in the cart. The condition "
        "is met, if for any of the chosen products, or any product in the "
        "selected categories, the minimum quantity is in the cart. If no "
        "products or categories are specified, the restriction applies to the "
        "sum of quantities in the cart."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:480
msgid   "Maximum"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:548
msgid   "Message"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:471
msgid   "Minimum"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:455
msgid   "No categories"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:44
msgid   "Product Archives"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:41
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:432
msgid   "Product Categories"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:45
msgid   "Product Pages"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:42
msgid   "Product Tags"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:454
msgid   "Product categories"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:390
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:415
msgid   "Products"
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:126
msgid   "Save"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:437
msgid   "Search for a category &hellip;"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:395
msgid   "Search for a product &hellip;"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:416
msgid   "Search for a product&hellip;"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:43
msgid   "Shop"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:516
msgid   "Show the <em>coupon description</em> for products on these pages:"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:530
msgid   "Show the <em>volume discount info</em> for products on these pages:"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:436
msgid   "Start typing to search for categories."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:394
msgid   "Start typing to search for products."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:495
msgid   "Sum categories"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:496
msgid   "Sum the number of units per category. If this is enabled, the "
        "quantity restrictions are based on the totals per category, instead "
        "of per individual product. This applies only to products in the "
        "selected categories, if any."
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:116
msgid   "The default inline style is:"
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:101
msgid   "Use inline styles"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:352
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:382
msgid   "Volume Discount"
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:53
#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:54
#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:92
msgid   "Volume Discount Coupons"
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:560
msgid   "Volume Discount Info"
msgstr  ""

#: lib/admin/class-woocommerce-volume-discount-coupons-admin.php:105
msgid   "When this option is enabled, the inline styles are used when the "
        "coupon description or volume discount info is displayed."
msgstr  ""

#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:472
#: lib/core/class-woocommerce-volume-discount-coupons-coupon.php:481
msgid   "no restriction"
msgstr  ""
