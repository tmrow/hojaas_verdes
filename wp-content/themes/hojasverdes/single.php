<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<?php $post_type=get_post_type(get_the_ID());?>
<main role="main" <?php post_class(); ?>>
		<section id="main" class="my-4">
			<div class="row">
				<div class="col col-md-6">
					<?php the_post_thumbnail() ?>
				</div>
				<div class="col col-md-6">
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>
				</div>
			</div>
		<section>
</main>

<?php endwhile; ?>



<?php else: ?>
	<!-- article -->
	<article>
		<h1><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h1>
	</article>
	<!-- /article -->
<?php endif; ?>

<?php get_footer(); ?>
