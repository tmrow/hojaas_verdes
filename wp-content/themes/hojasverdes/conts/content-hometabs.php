<div id="<?php echo $type; ?>" class="tab-container <?php if ($type=='actividad') {
    echo 'active';
} ?>">
  <?php
      $args = array(
          'post_type' => $type,
          'post_status' => 'publish',
          'ignore_sticky_posts'   => 1,
          'posts_per_page' => 8
          );
      $ids = array();
      $posts = new WP_Query($args);
      if ($posts->have_posts()) :
      while ($posts->have_posts()) : $posts->the_post();
           array_push($ids, get_the_ID());
      endwhile;
      endif;
    ?>
    <div class="w-100 text-center text-md-right pt-3 pb-2">
        <button class="btn btn-link list active">
          <span class="icon-list"></span>
        </button>
        <button class="btn btn-link map">
          <span class="icon-map"></span>
        </button>
    </div>
  <div class="row tab-boxes list active">
      <?php $i=0;
      foreach ($ids as $id) {
          set_query_var('type_r', $type);
          set_query_var('id', get_the_id());
          get_template_part('conts/content', 'single');
          if ($i==3) {
              echo '<div class="w-100 my-2 hidden-md-down"></div>';
          }
          $i++;
      } ?>
      <div class="w-100 my-2 hidden-md-down"></div>
      <div class="w-100 mt-1 hidden-lg-up"></div>

      <div class="col">
        <a href="<?php echo home_url(); ?>/actividades" class="btn btn-block btn-default bg-light-gray">VER MÁS
          <?php
          if ($type=='actividad') {
              echo 'ACTIVIDADES';
          }
          if ($type=='alojamiento') {
              echo 'ALOJAMIENTO';
          }
          if ($type=='evento') {
              echo 'EVENTOS';
          }
          ?>

        </a>
      </div>
    </div>
    <div class="tab-boxes map">
      <div class="acf-map" id="mapa" style="height: 350px; width: 100%;">
        <?php
          foreach ($ids as $id) {
              get_template_part('conts/content', 'maplist');
          }
        ?>
      </div>
    </div>
</div>
