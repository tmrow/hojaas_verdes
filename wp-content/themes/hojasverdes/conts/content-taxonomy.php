<main role="main" class="container-full">
  <section id="main-image" class="bg-primary">
    <?php echo get_the_post_thumbnail($id, 'lg', array('class' => 'center img-full')); ?>
    <div class="caption">
    	<div class="container">
        <div class="row caption-content">
          <div class="col-8">
            <h1>
          	<?php
                  if ($tax=='actividades') {
                      echo 'Actividades en ';
                  }
                  if ($tax=='eventos') {
                      echo 'Eventos en ';
                  }
                  if ($tax=='alojamientos') {
                      echo 'Alojamientos en ';
                  }
                  if ($tax=='restaurantes') {
                      echo 'Gastronomía en ';
                  }
                  if ($tax=='category') {
                      echo 'Noticias de ';
                  }
              ?>
          	 Chapadamalal
          	</h1>
          </div>
        </div>
    	</div>
    </div>
  </section>
  <?php
      $terms = get_terms(array(
        'taxonomy' => $tax,
        'hide_empty' => true
    ));
  ?>
  <div class="navbar-tax bg-light-gray hidden-sm-down" role="nav">
	  <div class="container">
      <div class="row">
		  <nav class="nav">
		  	<?php foreach ($terms as $term) {
      ?>
		  		<a class="nav-link text-uppercase py-3" href="#<?php echo $term->name; ?>">
            <?php echo $term->name; ?>
          </a>
		  	<?php

  } ?>
		  </nav>
    </div>
	  </div>
	</div>
	<section id="tax-1" class="mb-5">
		<div class="container">
      <?php foreach ($terms as $term) {
      set_query_var('type', $type);
      set_query_var('tax', $tax);
      set_query_var('term', $term->name);
      set_query_var('term_slug', $term->slug);
      get_template_part('conts/content', 'taxonomytabs');
  }
        ?>
		</div>
	</section>
</main>
