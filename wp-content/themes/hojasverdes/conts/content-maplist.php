<?php
$ubicacion = get_field('ubicacion', $id);
if ($ubicacion) {
    ?>
          <div class="marker" data-lat="<?php echo $ubicacion['lat']; ?>" data-lng="<?php echo $ubicacion['lng']; ?>">
              <strong><?php echo get_the_title($id); ?></strong>
              <p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php the_field('direccion', $id) ?></p>
          </div>


<?php

} ?>
