<div class="carousel-tax">
<?php if (ICL_LANGUAGE_CODE=='es') {
    ?>
<div class="col" data-toggle="modal" data-target="#search-modal">
  <div class="icon busqueda">
  </div>
  <div class="title">
    BÚSQUEDA
  </div>
</div>
<div class="col <?php if (is_tax('product_cat', 'especias')) {
        echo ' active';
    } ?>">
  <a href="<?php echo home_url() ?>/categoria/especias">
    <div class="icon especias"></div>
    <div class="title">
        ESPECIAS
    </div>
  </a>

</div>
<div class="col <?php if (is_tax('product_cat', 'condimentos')) {
        echo ' active';
    } ?>">
<a href="<?php echo home_url() ?>/categoria/condimentos">
  <div class="icon condimentos"></div>
  <div class="title">
      CONDIMENTOS
  </div>
</a>

</div>
<div class="col <?php if (is_tax('product_cat', 'deshidratados')) {
        echo ' active';
    } ?>">
  <a href="<?php echo home_url() ?>/categoria/deshidratados">
    <div class="icon deshidratados"></div>
    <div class="title">
        DESHIDRATADOS
    </div>
  </a>
</div>
<div class="col <?php if (is_tax('product_cat', 'frutos-secos')) {
        echo ' active';
    } ?>">
  <a href="<?php echo home_url() ?>/categoria/frutos-secos">
    <div class="icon frutos-secos"></div>
    <div class="title">
        FRUTOS SECOS
    </div>
  </a>
</div>
<div class="col <?php if (is_tax('product_cat', 'semillas')) {
        echo ' active';
    } ?>">
  <a href="<?php echo home_url() ?>/categoria/semillas">

    <div class="icon semillas"></div>
    <div class="title">
        SEMILLAS
    </div>
  </a>
</div>
<?php if (is_user_logged_in()) { ?>
  <div class="col <?php if (is_tax('product_cat', 'varios')) {
          echo ' active';
      } ?>">
    <a href="<?php echo home_url() ?>/categoria/varios">

      <div class="icon varios"></div>
      <div class="title">
          VARIOS
      </div>
    </a>
  </div>
<?php } ?>
<?php } elseif (ICL_LANGUAGE_CODE=='en') { ?>
  <div class="col" data-toggle="modal" data-target="#search-modal">
    <div class="icon busqueda">
    </div>
    <div class="title">
      SEARCH
    </div>
  </div>
  <div class="col <?php if (is_tax('product_cat', 'spices')) {
          echo ' active';
      } ?>">
    <a href="<?php echo home_url() ?>/category/spices">
      <div class="icon especias"></div>
      <div class="title">
          SPICES
      </div>
    </a>

  </div>
  <div class="col <?php if (is_tax('product_cat', 'condiments')) {
          echo ' active';
      } ?>">
  <a href="<?php echo home_url() ?>/category/condiments">
    <div class="icon condimentos"></div>
    <div class="title">
        CONDIMENTS
    </div>
  </a>

  </div>
  <div class="col <?php if (is_tax('product_cat', 'dehydrated')) {
          echo ' active';
      } ?>">
    <a href="<?php echo home_url() ?>/category/dehydrated">
      <div class="icon deshidratados"></div>
      <div class="title">
          DEHYDRATED
      </div>
    </a>
  </div>
  <div class="col <?php if (is_tax('product_cat', 'dry-fruits')) {
          echo ' active';
      } ?>">
    <a href="<?php echo home_url() ?>/category/dry-fruits">
      <div class="icon frutos-secos"></div>
      <div class="title">
          DRY FRUITS
      </div>
    </a>
  </div>
  <div class="col <?php if (is_tax('product_cat', 'seeds')) {
          echo ' active';
      } ?>">
    <a href="<?php echo home_url() ?>/category/seeds">
      <div class="icon semillas"></div>
      <div class="title">
          SEEDS
      </div>
    </a>
  </div>

<?php } ?>
</div>
<!-- Modal -->
<div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="searchform" method="get" action="<?php echo home_url(); ?>">
        <div class="modal-body">
            <div>
               <input type="text" class="form-control" name="s" id="s" size="15" placeholder="<?php if (ICL_LANGUAGE_CODE=='es') {echo'Ingresá tu búsqueda'; }else{echo 'Search';} ?>" /><br />
            </div>
            <div class="mt-2">
              <button type="button" class="btn btn-secondary float-left" data-dismiss="modal"><?php if (ICL_LANGUAGE_CODE=='es') {echo 'CERRAR'; }else{echo 'CLOSE';} ?></button>
              <input type="submit" value="<?php if (ICL_LANGUAGE_CODE=='es') {echo 'BUSCAR'; }else{echo 'SEARCH';} ?>" class="btn btn-primary float-right" />
              <div class="clearfix"></div>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>
