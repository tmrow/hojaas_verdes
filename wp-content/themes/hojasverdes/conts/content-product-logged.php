<?php
    $clases='col-12';
?>
<div class=" <?php echo $clases;?> box product logged">
  <div class="inner">
    <?php global $product; ?>
      <div class="tr">
        <div class="td imagen">
          <div class="pr-1">
            <a href="<?php the_permalink(); ?>">
              <?php if (has_post_thumbnail()) {
               echo get_the_post_thumbnail($id, 'thumbnail');
                   } else {
                       ?>
                              <img src="<?php echo get_template_directory_uri(); ?>/img/default.png" class="" />
                          <?php

                   }  ?>
            </a>
           </div>
        </div>
        <div class="td title relative">
          <div class="pr-3 vertical-align vertical-center <?php if (get_field('etiquetas')) { echo 'labeled'; } ?>">
            <h3>
              <!--<a href="<?php the_permalink(); ?>">-->
                <?php if (ICL_LANGUAGE_CODE=='es') {
                    the_title();
                } else {
                    the_field('nombre_en');
                }
                if (get_field('iva')==1) {
                  echo ' *';
                } elseif (get_field('iva')==2) {
                  echo ' **';
                }
                ?>
              <!--</a>-->
            </h3>
            <?php if (ICL_LANGUAGE_CODE=='es') {
               if (get_field('etiquetas')) {
                  if (get_field('etiquetas')=='Premium') {
                      $bg='txt-primary';
                  } elseif (get_field('etiquetas')=='Novedad') {
                      $bg='txt-warning';
                  } elseif (get_field('etiquetas')=='Máxima calidad') {
                      $bg='txt-success';
                  } else {
                      $bg='txt-primary';
                  } ?>
                <h5 class="<?php echo $bg; ?> text-uppercase"><?php the_field('etiquetas'); ?> </h5>
              <?php
              }
          }else {
            if (get_field('etiquetas_en')) {
               if (get_field('etiquetas_en')=='Premium') {
                   $bg='txt-primary';
               } elseif (get_field('etiquetas_en')=='News') {
                   $bg='txt-warning';
               } elseif (get_field('etiquetas_en')=='Top Quality') {
                   $bg='txt-success';
               } else {
                   $bg='txt-primary';
               } ?>
               <h5 class="<?php echo $bg; ?> text-uppercase"><?php the_field('etiquetas_en'); ?> </h5>
           <?php
           }
          }?>
          </div>
        </div>
        <div class="w-100"></div>
        <div class="td close hidden-lg-up small" id="small-<?php $product->name; ?>">
          <div class="row mb-1">
            <div class="col">
              <label class="price-select">PRECIO</label>
              <select class="weight-select">
                <?php if (get_field('_pricing_rules')){

                    // SI EL PRECIO ES DINAMICO TRAIGO LAS VARIABLES
                    $precio_dinamico = array_column(get_field('_pricing_rules'), 'rules');
                    $kg=$precio_dinamico[0][1]['from'];
                    $precio_kg=$precio_dinamico[0][1]['amount'];
                    $precio_total=$kg*$precio_kg; ?>

                    <?php if (get_field('bulto_cerrado')){ ?>
                      <option value="1"><?php echo $kg ?>kg</option>
                    <?php }else{ ?>
                      <option value="1">1kg</option>
                      <option value="10"><?php echo $kg ?>kg</option>
                    <?php } ?>
                <?php } else { ?>
                    <?php // SEMILLAS Y HARINAS: solo disponible por 25kg. Valor por kilo se incrementa $4. ?>
                    <?php if (has_term( 'semillas', 'product_cat', get_the_id() )){ ?>
                      <option value="1">1kg</option>
                      <option value="25">25kgs</option>
                    <?php }else{ ?>
                      <?php // Calculo normal para todos los productos ?>
                      <option value="1">1kg</option>
                      <option value="10">10kgs</option>
                      <option value="25">25kgs</option>
                      <option value="50">50kgs</option>
                    <?php } ?>
                  <?php } ?>

              </select>
            </div>
            <div class="col">
              <?php if (get_field('_pricing_rules')){
                 if (get_field('bulto_cerrado')){ ?>
                   <div class="showkg onekg">
                     <h5 class="mb-0"><strong>$ <?php echo $precio_total; ?></strong></h5>
                     <h5 class="mb-0"><small>($<?php echo $precio_kg; $price=$precio_kg; ?> x kg)</small></h5>
                   </div>
                <?php }else{ ?>
                    <div class="showkg onekg">
                      <h5 class="mb-0"><strong>$ <?php echo $product->get_price(); $price=$product->get_price(); ?></strong></h5>
                      <h5 class="mb-0"><small>($<?php echo $product->get_price(); ?> x kg)</small></h5>
                    </div>
                    <div class="showkg tenkg">
                      <h5 class="mb-0"><strong>$ <?php echo $precio_total; ?></strong></h5>
                      <h5 class="mb-0"><small>($<?php echo $precio_kg; ?> x kg)</small></h5>
                    </div>
                <?php } ?>
              <?php } else { ?>
                <?php // SEMILLAS Y HARINAS: solo disponible por 25kg. Valor por kilo se incrementa $4. ?>
                <?php if (has_term( 'semillas', 'product_cat', get_the_id() )){ ?>
                  <div class="showkg onekg">
                    <h5 class="mb-0"><strong>$ <?php echo $product->get_price(); $price=$product->get_price(); $price_desc=$price-4; ?></strong></h5>
                    <h5 class="mb-0"><small>($<?php echo $product->get_price(); ?> x kg)</small></h5>
                  </div>
                  <div class="showkg twentykg">
                    <h5 class="mb-0"><strong>$ <?php echo $price_desc*25; ?></strong></h5>
                    <h5 class="mb-0" ><small>($<?php echo $price_desc; ?> x kg)</small></h5>
                  </div>
                <?php } else { ?>
              <div class="showkg onekg">
                <h5 class="mb-0"><strong>$ <?php echo $product->get_price(); $price=$product->get_price(); ?></strong></h5>
                <h5 class="mb-0"><small>($<?php echo $product->get_price(); ?> x kg)</small></h5>
              </div>
              <div class="showkg tenkg">
                <h5 class="mb-0"><strong>$ <?php $price10=$price*0.92; echo $price*10; ?></strong></h5>
                <h5 class="mb-0"><small>($<?php echo $price10; ?> x kg)</small></h5>
              </div>
              <div class="showkg twentykg">
                <h5 class="mb-0"><strong>$ <?php $price10=$price*0.89; echo $price10*25; ?></strong></h5>
                <h5 class="mb-0" ><small>($<?php echo $price10; ?> x kg)</small></h5>
              </div>
              <div class="showkg fiftykg">
                <h5 class="mb-0"><strong>$ <?php $price10=$price*0.85; echo $price10*50; ?></strong></h5>
                <h5 class="mb-0"><small>($<?php  echo $price10; ?> x kg)</small></h5>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
            <i class="fa fa-angle-down close-icon hidden-sm-up" aria-hidden="true"></i>
          </div>
          <div class="row">
            <div class="col hello">
              <?php if ($product->is_in_stock() and $price!=0) : ?>

                <?php do_action('woocommerce_before_add_to_cart_form'); ?>
                  <form class="cart" method="post" enctype='multipart/form-data'>
                    <?php
                      woocommerce_quantity_input(array(
                        'min_value'   => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                        'max_value'   => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                        'input_value' => isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : $product->get_min_purchase_quantity(),
                      ));
                    ?>
                    <span class="float-left kgq pr-1 txt-mid-gray">
                      <?php if (get_field('unidad')){
                            echo 'unids';
                          }else{
                            echo 'kg';
                          }
                       ?>
                    </span>
                    <button type="submit"
                        <?php if (get_field('bulto_cerrado')){
                          echo 'data-quantity="'.$kg.'"';
                        }else{
                          echo 'data-quantity="1"';
                        }
                        ?>
                        data-product_id="<?php echo $product->id; ?>"
                        class="button ajax_add_to_cart add_to_cart_button product_type_simple btn btn-warning float-left ">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                  </form>
                <?php do_action('woocommerce_after_add_to_cart_form'); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?php if (get_field('_pricing_rules')){
          $precio_dinamico = array_column(get_field('_pricing_rules'), 'rules');
          $kg=$precio_dinamico[0][1]['from'];
          $precio_kg=$precio_dinamico[0][1]['amount'];
          $precio_total=$kg*$precio_kg;
           ?>
          <div class="td weight hidden-md-down">
            <?php if (!get_field('bulto_cerrado')){ ?>
                <div class="kg">
                  <h3><?php if (get_field('unidad')){
                        echo 'unid';
                      }else{
                        echo '1KG';
                      }
                   ?>  </h3>
                  <h5><small>$<?php echo $product->get_price(); ?></small></h5>
                  <h5> <?php $price=$product->get_price(); ?></h5>
                </div>
                <div class="kg">
                  <h3><?php echo $kg; ?>KG</h3>
                  <h5><small>$<?php echo $precio_kg; ?></small></h5>
                  <h5>$<?php echo $precio_total; ?></h5>
                </div>
            <?php }else{ ?>
              <?php if (get_field('unidad')){ ?>
                <div class="kg">
                  <h3>unid</h3>
                  <h5>$<?php echo $product->get_price(); $price=$product->get_price(); ?></h5>
                </div>
              <?php }else{ ?>
                <div class="kg">
                  <h3><?php echo $kg; ?>KG</h3>
                  <h5><small>$<?php echo $precio_kg; ?></small></h5>
                  <h5>$<?php echo $precio_total; ?></h5>
                </div>
              <?php } ?>
            <?php } ?>

          </div>
        <?php  }else{ ?>
            <div class="td weight hidden-md-down">
              <?php // SEMILLAS Y HARINAS: solo disponible por 25kg. Valor por kilo se incrementa $4. ?>
              <?php if (has_term( 'semillas', 'product_cat', get_the_id() )){ ?>
                <div class="kg">
                  <h3><?php if (get_field('unidad')){
                        echo 'unid';
                      }else{
                        echo '1KG';
                      }
                   ?>  </h3>
                  <h5><small>$<?php echo $product->get_price(); ?></small></h5>
                  <h5><?php $price=$product->get_price(); $price_desc=$price-4;?></h5>
                </div>
                <div class="kg">
                  <h3>25KG</h3>
                  <h5><small>$<?php echo $price_desc; ?></small></h5>
                  <h5>$<?php echo $price_desc*25; ?></h5>
                </div>
              <?php } else { ?>
                <div class="kg">
                  <h3><?php if (get_field('unidad')){
                        echo 'unid';
                      }else{
                        echo '1kg';
                      }
                   ?>  </h3>
                  <h5><small>$<?php echo $product->get_price(); ?></small></h5>
                  <h5> <?php $price=$product->get_price(); ?></h5>
                </div>
                <?php if(!has_term( 'frutos-secos', 'product_cat', get_the_id() )) { ?>
                    <div class="kg">
                      <h3>10KG</h3>
                      <h5><small>$<?php $price10=$price*0.92; echo $price10; ?></small></h5>
                      <h5>$<?php echo $price10*10; ?></h5>
                    </div>
                    <div class="kg">
                      <h3>25KG</h3>
                      <h5><small>$<?php $price10=$price*0.89; echo $price10; ?></small></h5>
                      <h5>$<?php echo $price10*25; ?></h5>
                    </div>
                    <div class="kg pr-3">
                      <h3>50KG</h3>
                      <h5><small>$<?php $price10=$price*0.85; echo $price10; ?></small></h5>
                      <h5>$<?php echo $price10*50; ?></h5>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
        <div class="td quantity hidden-md-down">
          <?php if ($product->is_in_stock() and $price!=0 or $precio_kg!=0) : ?>

            <?php do_action('woocommerce_before_add_to_cart_form'); ?>

            <form class="cart <?php if (get_field('bulto_cerrado')){ echo 'bulto-cerrado'; }?>" method="post" enctype='multipart/form-data'>
              <?php
                /**
                 * @since 2.1.0.
                 */
                //do_action('woocommerce_before_add_to_cart_button');

                /**
                 * @since 3.0.0.
                 */
                //do_action('woocommerce_before_add_to_cart_quantity');

                woocommerce_quantity_input(array(
                  'min_value'   => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                  'max_value'   => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                  'input_value' => isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : $product->get_min_purchase_quantity(),
                ));

                /**
                 * @since 3.0.0.
                 */
                //do_action('woocommerce_after_add_to_cart_quantity');
              ?>
              <span class="float-left kgq pr-1 txt-mid-gray">
                <?php if (get_field('unidad')){
                      echo 'unid';
                    }else{
                      echo 'kg';
                    }
                 ?>
              </span>
              <button type="submit"
                  <?php if (get_field('bulto_cerrado')){
                    echo 'data-quantity="'.$kg.'"';
                  }else{
                    echo 'data-quantity="1"';
                  }
                  ?>
                  data-product_id="<?php echo $product->id; ?>"
                  class="button ajax_add_to_cart add_to_cart_button product_type_simple btn btn-warning float-left ">
                  <i class="fa fa-plus" aria-hidden="true"></i>
              </button>

              <!--<button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="btn btn-warning pull-left text-uppercase"><i class="fa fa-plus" aria-hidden="true"></i></button>-->

              <?php
                /**
                 * @since 2.1.0.
                 */
                //do_action('woocommerce_after_add_to_cart_button');
              ?>
            </form>

            <?php do_action('woocommerce_after_add_to_cart_form'); ?>

          <?php endif; ?>
        </div>
        <div class="clearfix">

        </div>
      </div>
      </div>
</div>
