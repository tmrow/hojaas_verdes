	<div class="tax-tabs" id="<?php echo $term ?>">
		<div class="tax-head">
		<h2 class="text-uppercase">
			<?php echo $term ?>
		</h2>
		<?php
      $args = array(
          'post_type' => $type,
          'post_status' => 'publish',
          'posts_per_page' => 8,
                    'tax_query' => array(
                        array(
                            'taxonomy' => $tax, //or tag or custom taxonomy
                            'field' => 'name',
                            'terms' => array($term_slug)
                        )
                    )
          );
      $ids = array();
      $posts = new WP_Query($args);
      if ($posts->have_posts()) :
      while ($posts->have_posts()) : $posts->the_post();
           array_push($ids, get_the_ID());
      endwhile;
      endif;
    ?>
    <div class="pull-right">
					<button class="btn btn-link list active">
						<span class="icon-list"></span>
					</button>
					<button class="btn btn-link map">
						<span class="icon-map"></span>
					</button>
    </div>
		<div class="clearfix">

		</div>
	</div>
	<div class="w-100"></div>
	<div class="tab-container mt-4 ">
  <div class="row tab-boxes list active">
      <?php $i=0;
      foreach ($ids as $id) {
          set_query_var('type_r', $type);
          get_template_part('conts/content', 'single');
          if ($i==3) {
              echo '<div class="w-100 my-2 hidden-md-down"></div>';
          }
          $i++;
      } ?>
			<div class="w-100 my-2 hidden-md-down"></div>
      <div class="w-100 mt-1 hidden-lg-up"></div>

			<div class="col">
        <a href="<?php echo home_url(); ?>/<?php if ($tax=='category') {
          echo 'noticias';
      } else {
          echo $tax;
      } ?>/<?php echo $term_slug; ?>" class="btn btn-block btn-default bg-light-gray text-uppercase">
					VER MÁS <?php echo $term ?>
        </a>
      </div>

    </div>
    <div class="tab-boxes map">
      <div class="acf-map" id="mapa" style="height: 350px; width: 100%;">
        <?php
          foreach ($ids as $id) {
              set_query_var('id', $id);
              get_template_part('conts/content', 'maplist');
          }
        ?>
      </div>
    </div>
	</div>
</div>
