<?php
    $clases='col-6 col-lg-3';
?>
<div class=" <?php echo $clases;?> mb-3 box iphone">
  <div class="inner">
    <?php if (ICL_LANGUAGE_CODE=='es') {
       if (get_field('etiquetas')) {
          if (get_field('etiquetas')=='Premium') {
              $bg='bg-primary';
          } elseif (get_field('etiquetas')=='Novedad') {
              $bg='bg-warning';
          } elseif (get_field('etiquetas')=='Máxima calidad') {
              $bg='bg-success';
          } else {
              $bg='bg-primary';
          } ?>
      <div class="label <?php echo $bg; ?>">
        <?php the_field('etiquetas'); ?>
      </div>
      <?php
      }
  }else {
    if (get_field('etiquetas_en')) {
       if (get_field('etiquetas_en')=='Premium') {
           $bg='bg-primary';
       } elseif (get_field('etiquetas_en')=='News') {
           $bg='bg-warning';
       } elseif (get_field('etiquetas_en')=='Top Quality') {
           $bg='bg-success';
       } else {
           $bg='bg-primary';
       } ?>
   <div class="label <?php echo $bg; ?>">
     <?php the_field('etiquetas_en'); ?>
   </div>
   <?php
   }
  }?>
    <div class="image">
      <a href="<?php echo get_the_permalink($id); ?>" title="<?php the_title(); ?>">
        <?php if (has_post_thumbnail()) {
          add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
          echo get_the_post_thumbnail($id, 'medium');
          remove_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
} else {
    ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/default.png" class="center" />
        <?php

}  ?>
    </a>
    </div>

  <a href="<?php echo get_the_permalink($id); ?>" title="<?php the_title(); ?>">
  <div class="caption">
    <H4>
      <?php if (ICL_LANGUAGE_CODE=='es') {
          echo get_the_title($id);
      } else {
          the_field('nombre_en', $id);
      } ?>
    </H4>
  </div>
  </a>
    </div>
</div>
