<?php
if (!isset($rel)) {
    $rel=0;
};
if (!isset($feat)) {
    $feat=0;
};
if (get_post_type($id)=='actividad') {
    $tax='actividades';
} elseif (get_post_type($id)=='gastronomia') {
    $tax='restaurantes';
} elseif (get_post_type($id)=='alojamiento') {
    $tax='alojamientos';
} elseif (get_post_type($id)=='eventos') {
    $tax='eventos';
} elseif (get_post_type($id)=='post') {
    $tax='category';
} else {
    $tax='';
}
if ($rel==1) {
    $clases='col-12 col-lg-4';
} elseif ($rel==0) {
    $clases='col-12 col-sm-12 col-md-6 col-lg-3';
}
?>
<div class=" <?php echo $clases;  if ($feat==1) {
    echo 'mb-3';
} ?>  box">
  <div class="inner">
    <a href="<?php echo get_the_permalink($id); ?>" title="<?php the_title(); ?>">
  <?php if (has_post_thumbnail()) {
    if ($feat=='1') {
        echo get_the_post_thumbnail($id, 'lg', array('class' => 'center img-full'));
    } else {
        echo get_the_post_thumbnail($id, 'thumbnail', array('class' => 'center img-full'));
    }
} else {
    echo '<img src="http://placehold.it/400x330/3da8b9" class="center img-full"/>';
}
  ?>
  </a>
  <a href="<?php echo get_the_permalink($id); ?>" title="<?php the_title(); ?>">

  <div class="caption">
  <?php if (get_post_type($id)=='evento') {
      if (get_field('evento_inicio', $id)) {
          ?>
        <span class="bg-primary event"><i class="fa fa-calendar" aria-hidden="true"></i>
         <?php
         the_field('evento_inicio', $id);
          if (get_field('evento_finalizacion', $id)) {
              echo ' al ';
              the_field('evento_finalizacion', $id);
          } ?>
        </span>
      <?php

      }
  } else {
      ?>
      <h5 class="text-uppercase">
        <?php
          $terms = get_the_terms($id, $tax);
      if (is_search()) {
          echo $tax;
          echo ' ';
      }
      if ($terms!="") {
          echo $terms[0]->name;
      } ?>
      </h5>
    <?php

  } ?>
    <H3><?php echo get_the_title($id); ?></H3>
  </div>
  </a>
    </div>
</div>
