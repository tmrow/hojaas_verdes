<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if (wp_title('', false)) {
    echo ' :';
} ?> <?php bloginfo('name'); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<!--<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/090095a0-2a37-4d59-a33a-cda762291703.css"/>-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
		<script src='https://www.google.com/recaptcha/api.js'></script>

	<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
		<!-- wrapper -->
			<!-- header -->
			<header class="header clear" role="banner">
				<nav id="main-navbar" class="fixed-top yamm hidden-md-down">
							<ul class="nav justify-content-center">
								<li class="nav-item">
									<?php if (ICL_LANGUAGE_CODE=='es') { ?>
									<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/categoria/especias"><?php _e('Products', 'woocommerce'); ?></a>
									<?php } else { ?>
										<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/category/spices"><?php _e('Products', 'woocommerce'); ?></a>
									<?php	} ?>
								</li>
								<?php if (ICL_LANGUAGE_CODE=='es') { ?>
									<li class="nav-item">
										<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/contacto">
											CONTACTO
										</a>
									</li>
								<?php }  ?>
								<li class="nav-item" style="margin-top:-20px;">
								 <a class="nav-link" href="#">
									 <a href="<?php echo home_url(); ?>">
										 <img src="<?php echo get_template_directory_uri(); ?>/img/logo-hojas-verdes.svg" />
									 </a>
								 </a>
							 </li>
							 <?php if (ICL_LANGUAGE_CODE=='es') { ?>
							 <?php if (!is_user_logged_in()) {
    ?>
								 <li class="nav-item">
 									<a class="nav-link text-uppercase ml-1" href="<?php echo home_url() ?>/mi-cuenta"><?php _e('Register', 'woocommerce'); ?></a>
 								</li>
 								<li class="nav-item">
 									<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/mi-cuenta"><?php _e('Login', 'woocommerce'); ?></a>
 								</li>
							<?php

						} else {
    				?>
							<li class="nav-item dropdown yamm-fw cart-drop">
								<a class="nav-link dropdown-toggle text-uppercase" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>
											<?php _e('Order', 'woocommerce'); ?>
											<?php /*echo sprintf ( _n( '%d item', '%d items', count(WC()->cart->get_cart()) ), count(WC()->cart->get_cart()) ); ?> - <?php echo WC()->cart->get_cart_total(); */?>
								 </a>
								 <div class="dropdown-menu">
 									<div class="container">
 										<div class="row justify-content-md-center">
 										 <div class="col-8">
 												 <div class="row cart_totals">
													 <?php global $woocommerce; $suma=array(); $valor=""; ?>

 											 <?php do_action('woocommerce_review_order_before_cart_contents');
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			//print_r($cart_item);
        $_product     = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);

        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
            ?>
 												 <div class="col-8">
 														 <?php echo apply_filters('woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity mr-1">' . sprintf('%s', $cart_item['quantity']) . ' kg</strong>', $cart_item, $cart_item_key); ?>

 														 <?php echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;'; ?>
 														 <?php echo WC()->cart->get_item_data($cart_item); ?>
 													 </div>
 													 <div class="col-4 text-right">
														<?php echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); ?>
 													 </div>



 																 <?php

																		        }
																		    }

																		    do_action('woocommerce_review_order_after_cart_contents'); ?>

													<?php if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) { ?>

																		 <div class="w-100">
																		 	 <hr />
																		 </div>



 													 <div class="order-total col-12 text-right">
 														 <strong> SUBTOTAL:</strong>
 														 <?php echo WC()->cart->get_cart_total(); ?>
														 <small>+ IVA</small>
 													 </div>

 													 <div class="col-12 mt-2">
 														 <a href="<?php echo home_url(); ?>/checkout" class="float-right">
 															 <button class="btn btn-warning text-uppercase"> <?php _e('Checkout', 'woocommerce'); ?></button>
 														 </a>
														 <a href="<?php echo home_url(); ?>/cart" class="float-left">
 															 <button class="btn btn-danger text-uppercase"> <?php _e('Cart', 'woocommerce'); ?></button>
 														 </a>
														 <div class="clearfix"></div>
 													 </div>
													 <?php }else{ ?>
														 <div class="text-center mx-auto">
															 <p>
															 		No hay ningún producto.
															 </p>
															 <div class="w-100">
															 </div>
															 <?php
															 $usuario=get_user_meta(get_current_user_id());
															 if ($usuario['pw_user_status'][0]=='approved') { ?>
															 <a href="<?php echo home_url() ?>/categoria/semillas" class="pl-0"><button class="btn btn-warning text-center">COMENZÁ TU COMPRA</button></a>
															 <?php } ?>
														 </div>
													 <?php } ?>
 													 <?php do_action('woocommerce_review_order_after_order_total'); ?>

 												 </div>

 										 </div>
 									 </div>
 									</div>
 								</div>
						 </li>
						 <li class="nav-item dropdown yamm-fw menu-drop">
						 	<a class="nav-link dropdown-toggle text-uppercase" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						 		 <?php _e('My account', 'woocommerce'); ?>
						 	 </a>
							 <div class="dropdown-menu">
 							 <div class="container">
 								 <div class="row justify-content-md-center">
 									<div class="col-8">
 											<div class="row cart_totals right">
						 	<?php foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
						 	 <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>" class="dropdown-item text-uppercase"><?php echo esc_html($label); ?></a>
						  <?php endforeach; ?>
						  </div>
							 </div>
							  </div>
								 </div>
								  </div>
						 </li>

							 <?php

} ?>
<?php }else { ?>
	<li class="nav-item">
		<a class="nav-link text-uppercase" href="<?php echo home_url() ?>/contact">
			CONTACT
		</a>
	</li>
<?php } ?>
<li class="nav-item">
	<?php do_action('wpml_add_language_selector'); ?>
</li>
							</ul>

				</nav>
				<nav class="hidden-lg-up bg-primary py-2 fixed-top">
					<div class="container">
						<div class="row">
							<div class="col text-center">
								<a href="<?php echo home_url() ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/logo.svg" class="head-icon"  alt="phone logo"/>
								</a>
							</div>
							<div class="col text-center">
								<a href="<?php echo home_url() ?>/categoria/especias">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/productos.svg" class="head-icon" alt="productos"/>
								</a>
							</div>
							<div class="col text-center">
								<?php if (is_user_logged_in()) { ?>
									<a href="<?php echo home_url() ?>/cart" class="relative">
										<span class="counter"><?php echo count(WC()->cart->get_cart()); ?></span>
										<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/carrito.svg" class="head-icon" alt="productos"/>
									</a>
								<?php }else{ ?>
									<a href="<?php echo home_url() ?>/mi-cuenta" class="relative">
										<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/user.svg" class="head-icon" alt="productos"/>
									</a>
								<?php }?>
							</div>
							<div class="col text-center">
								<a href="<?php echo home_url() ?>/contacto">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/contacto.svg" class="head-icon"  alt="productos"/>
								</a>
							</div>
							<?php if (is_user_logged_in()) { ?>
									<div class="col text-center">
										<a href="#" class="navbar-toggle" data-toggle="offcanvas" data-target="#smallnav" data-canvas="body">
											<img src="<?php echo get_template_directory_uri(); ?>/img/icons/xs/menu.svg" class="head-icon" alt="productos"/>
										</a>
									</div>
							<?php } ?>
						</div>
					</div>
				</nav>
				<nav id="smallnav" class="navmenu navmenu-default navmenu-fixed-right offcanvas" role="navigation">
					<ul class="nav flex-column">
							<?php foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
								<li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?> nav-item">
									<a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>" class="nav-link"><?php echo esc_html($label); ?></a>
								</li>
							<?php endforeach; ?>
						</ul>
				</nav>
    	</header>
