<?php get_header(); ?>
<main role="main" class="container-full">
  <?php if (!is_user_logged_in() or $usuario['pw_user_status'][0]!='approved') { ?>
    <section id="main-image" class="bg-primary mid">
      <div class="image-cover">
        <?php echo wp_get_attachment_image(445, 'full', '', array( "class" => "img-full" ));?>
      </div>
      <div class="image-cover"></div>
      <div class="caption">
        <h1>
          <?php if (ICL_LANGUAGE_CODE=='es') { ?>
            NUESTROS PRODUCTOS
          <?php }else{ ?>
              OUR PRODUCTS
          <?php } ?>
        </h1>
        <h3>
          <small class="text-lowercase">
            <?php if (ICL_LANGUAGE_CODE=='es') { ?>
              Ingredientes que importan
            <?php }else{ ?>
              Ingredients that matter
            <?php } ?>
          </small>
        </h3>
      </div>
    </section>

  <?php } else { ?>
      <section id="main-image" class="bg-primary mid">
        <?php echo wp_get_attachment_image(445, 'full', '', array( "class" => "img-full center" ));?>
        <div class="image-cover"></div>
        <div class="caption">
            <h1>
              PEDIDOS ONLINE
            </h1>
            <h3>
              <small class="text-lowercase">Acceso a clientes</small>
            </h3>
        </div>
      </section>
  <?php }  ?>
  <section id="tax-1">
    <div class="container">
      <div class="tax-tabs" id="<?php echo $term->slug ?>">
        <div class="tax-head my-4">
        <div class="row">
          <?php get_template_part('conts/content', 'catlist'); ?>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="w-100"></div>
      <div class="row tab-boxes list">
        <?php get_template_part('loop'); ?>
      </div>
    </div>
    </div>
    <?php ?>
    <div class="pagination container mb-4">
    <?php
      $term = $_GET['s'];
      echo do_shortcode('[ajax_load_more post_type="product" offset="24" search="'. $term .'" button_label="Ver más" button_loading_label="Cargando productos"]');
      ?>
    </div>
  </section>
</main>
<?php get_footer(); ?>
