<!-- pagination -->
<div class="container mb-4">

			<?php
			$term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
			 $usuario=get_user_meta(get_current_user_id());
			if (!is_user_logged_in() or $usuario['pw_user_status'][0]!='approved') {
			echo do_shortcode('[ajax_load_more post_type="product" offset="12" taxonomy="product_cat" taxonomy_terms="'.$term->slug.'" taxonomy_operator="IN" posts_per_page="12" order="ASC" orderby="menu_order" button_label="Ver más" meta_key="oculto" meta_value="1" meta_compare="NOT IN"  button_loading_label="Cargando productos"]');
			} else {

				echo do_shortcode('[ajax_load_more post_type="product" order="ASC" offset="12" orderby="menu_order" taxonomy="product_cat" taxonomy_terms="'.$term->slug.'" taxonomy_operator="IN" posts_per_page="12" button_label="Ver más" button_loading_label="Cargando productos"]');
		}
    ?>
</div>
<!-- /pagination -->
