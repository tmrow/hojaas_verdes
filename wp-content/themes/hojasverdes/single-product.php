<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<?php $post_type=get_post_type(get_the_ID());?>
<main role="main" class="pt-4">
		<section id="main" class="mt-5 mb-4 container">
			<div class="row">
				<div class="col-12 col-md-5">
					<span class='zoom' id='ex1'>
						<?php the_post_thumbnail() ?>
						<!--<img src='daisy.jpg' width='555' height='320' alt='Daisy on the Ohoopee'/>-->
					</span>



				</div>
				<div class="col-12 col-md-7">
					<h3 class="text-primary"><strong>
						<?php if (ICL_LANGUAGE_CODE=='es') {
								the_title();
						} else {
								the_field('nombre_en');
						} ?>
						</strong>
				</h3>
						<?php if (ICL_LANGUAGE_CODE=='es') {
									the_field('descripcion');
							} else {
									the_field('descripcion_en');
							} ?>
					<div class="row woocomerce pt-2">
							<?php if (!is_user_logged_in()) { ?>
								<div class="col-12">
									<?php if (ICL_LANGUAGE_CODE=='es') { ?>
										<a href="<?php echo home_url(); ?>/mi-cuenta"><button class="btn btn-warning">HACE TU PEDIDO</button></a>
									<?php }else{ ?>
										<a href="<?php echo home_url(); ?>/contact"><button class="btn btn-warning">
Ask about this product</button></a>
									<?php } ?>
								</div>
							<?php }else{ ?>
								<div class="col-12">
									<a href="<?php echo home_url(); ?>?s=<?php the_title(); ?>#tax-1"><button class="btn btn-warning">
Comprar</button></a>
								</div>
							<?php } ?>
						</div>
				</div>
			</div>
			<div class="w-100">

			</div>
			<div class="row mt-4">
				<div class="col-12">
					<h2><small class="text-primary">
							<?php if (ICL_LANGUAGE_CODE=='es') { ?>
									Productos similares
							<?php } else { ?>
									Similar products
							<?php } ?>
								</small></h2>
				</div>
				<div class="w-100"></div>
				<?php
				// get the custom post type's taxonomy terms

				$custom_taxterms = wp_get_object_terms( $post->ID, 'product_cat', array('fields' => 'ids') );
				// arguments
				$args = array(
				'post_type' => 'product',
				'post_status' => 'publish',
				'posts_per_page' => 4, // you may edit this number
				'orderby' => 'rand',
				'meta_key'		=> 'oculto',
				'meta_value'	=> '0',
				'tax_query' => array(
				    array(
				        'taxonomy' => 'product_cat',
				        'field' => 'id',
				        'terms' => $custom_taxterms
				    )
				),
				'post__not_in' => array ($post->ID),
				);
				$related_items = new WP_Query( $args );
				// loop over query
				if ($related_items->have_posts()) :

				while ( $related_items->have_posts() ) : $related_items->the_post();
				get_template_part( 'conts/content', 'product' );
				endwhile;
				endif;
				// Reset Post Data
				wp_reset_postdata();
				?>
			</div>
		<section>
</main>
<?php endwhile; ?>
<?php else: ?>
	<!-- article -->
	<article>
		<h1><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h1>
	</article>
	<!-- /article -->
<?php endif; ?>

<?php get_footer(); ?>
