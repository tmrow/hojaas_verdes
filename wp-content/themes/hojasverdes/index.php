<?php get_header(); ?>
<?php wp_reset_query();?>
	<main role="main" class="container-full">
		<section id="main-image" class="fullheight fixed">
			<div id="home-slide" class="slider fullheight">
				<div class="slogan">
					<?php if (ICL_LANGUAGE_CODE=='es') {
    ?>
					<img src="<?php echo get_template_directory_uri() ?>/img/comparte-el-sabor.png" class="img-fluid" />
					<?php

} else {
    ?>
						<img src="<?php echo get_template_directory_uri() ?>/img/share-the-flavor.png" class="img-fluid" />
					<?php

} ?>
					<div class="w100">

					</div>
					<?php if (ICL_LANGUAGE_CODE=='es') {  ?>
						<a href="<?php echo home_url(); ?>/categoria/especias">
							<button class="btn btn-primary">
	    					DESCUBRÍ NUESTROS PRODUCTOS
							</button>
						</a>
					<?php } else { ?>
						<a href="<?php echo home_url(); ?>/category/spices">
							<button class="btn btn-primary">
	    					KNOW OUR PRODUCTS
							</button>
						</a>
					<?php } ?>

				</div>
				<div class="image-cover">
					<?php
					echo wp_get_attachment_image(365, 'full', true, array( "class" => "img-full" ));
					 ?>
				</div>
				<!--<div class="image-cover" style="background-image:url('<?php echo wp_get_attachment_image_url(365, 'full'); ?>')"></div>-->
			</div>
		</section>
		<section id="presentation" class="bg-warning">
			<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-12 col-md-12 text-center my-4">
							<div class="row">
								<div class="col-12 text-white mb-4">
									<?php if (ICL_LANGUAGE_CODE=='es') {
    ?>
										<p class="mb-1">SOMOS UNA EMPRESA FAMILIAR ESPECIALIZADA EN</p>
										<h2 class="mb-0">ESPECIAS, CONDIMENTOS Y <br />VEGETALES DESHIDRATADOS</h2>
										<?php

} else {
    ?>
											<p class="mb-1">WE ARE A FAMILY COMPANY SPECIALIZED IN</p>
											<h2 class="mb-0">
SPICES, CONDIMENTS AND <br />DEHYDRATED VEGETABLES</h2>
								<?php

} ?>
								</div>
								<div class="w100"></div>
								<div class="col-12 col-md-6" id="empresa" >
									<div class="image-box">
										<div class="image-cover" style="background-image:url('<?php echo wp_get_attachment_image_url(441, 'large'); ?>')"></div>
										<div class="caption text-white">
											<h3 class="mb-1">
												<?php if (ICL_LANGUAGE_CODE=='es') {
    echo "NUESTRA EMPRESA";
} else {
    echo "OUR COMPANY";
} ?>
												</h3>
											<p>
												<?php if (ICL_LANGUAGE_CODE=='es') {
    echo "Hojas Verdes es una empresa familiar  fundada en 1970 por Rogelio Valle.
														Su legado, después de casi 50 años, ha crecido y se ha perfeccionado en los diferentes
														procesos de trabajo y calidad.";
} else {
    echo "Hojas Verdes is a family business founded in 1970 by Rogelio Valle.
															His legacy, after almost 50 years, has grown and improved in the different
															processes of work and quality.";
} ?>
											</p>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-6" id="calidad">
									<div class="image-box">
										<div class="image-cover" style="background-image:url('<?php echo wp_get_attachment_image_url(439, 'large'); ?>')"></div>
										<div class="caption text-white">
											<h3 class="mb-1">
												<?php if (ICL_LANGUAGE_CODE=='es') {
    echo "CALIDAD";
} else {
    echo "QUALITY";
} ?>
											</h3>
											<p>
												<?php if (ICL_LANGUAGE_CODE=='es') {
    echo "Los aromas, las texturas y lo sabores de nuestros productos se deben al cuidado riguroso de los procesos de calidad preservados desde su origen, en su proceso de maduración y acopio hasta su distribución final.											</p>
														";
} else {
    echo "
																													The flavors, textures and tastes of our products are due to the care
																													rigorous quality processes preserved from its origin until its final distribution.";
} ?>
										</div>
									</div>
								</div>
								<div class="w100"></div>
								<div class="col col-md-12 mt-3" id="online">
									<div class="image-box">
										<div class="image-cover" style="background-image:url('<?php echo wp_get_attachment_image_url(443, 'large'); ?>')"></div>
										<div class="caption text-white">
											<h3 class="mb-1">
												<?php if (ICL_LANGUAGE_CODE=='es') {
    echo "PEDIDOS ONLINE";
} else {
    echo "ONLINE ORDERS";
} ?>

											</h3>
											<div class="row">
												<div class="col col-lg-6">
													<?php if (ICL_LANGUAGE_CODE=='es') {
			echo "	<p>
					Te presentamos la tienda online de especias con el catalogo de productos más completo de América Latina.
					Hacé tus pedidos en nuestra plataforma, nuestro equipo iniciará la orden y coordinará el pedido para su entrega.</p> <p>
					Hacemos envíos a todo el país. También puede retirarlo en nuestro Centro de Distribución en Av. General Paz 7771, Ciudadela Norte, Gran Buenos Aires.		</p>	";
		} else {
			echo "
		<p>
		We present the online spice shop with the most complete product catalog in Latin America.
		Make your orders on our platform, our team will initiate the order and coordinate the delivery. </p> <p>
		We do sends to all the world. </p>";
		} ?>
												</div>
											</div>

			<a href="<?php echo home_url() ?>/mi-cuenta"><button class="btn btn-warning text-uppercase mb-1">
								<?php if (ICL_LANGUAGE_CODE=='es') {
				echo "Registrate ahora";
				} else {
				echo "Register now";
				} ?>
			</button></a>

											</div>
									</div>
								</div>
							</div>

			      </div>
					</div>
			</div>
		</section>
		<section id="consultas" class="bg-white">
			<div class="container py-5">
				<div class="row">
					<div class="col-12">
						<h3 class="text-center mb-4">
							<?php if (ICL_LANGUAGE_CODE=='es') {
									echo "CONSULTAS";
									} else {
									echo "CONTACT";
									} ?>
						</h3>
					</div>
					<div class="w100">

					</div>
					<div class="col-12 col-md-6 mapa" style="border-radius: 0 30px 0 30px; overflow: hidden; height: 300px;">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.158741358089!2d-58.53312948476968!3d-34.62542858045383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcc822029185ef%3A0x65906368a1680709!2sAv.+Gral.+Paz+7771%2C+Ciudadela%2C+Buenos+Aires!5e0!3m2!1ses-419!2sar!4v1498825133590" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="col-12 col-md-6 txt-mid-gray pt-5">
						<p>
							<i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-3075  <br /><i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4657-5972
							 <br /> <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-0539
						</p>
						<p>
								<i class="fa fa-whatsapp" aria-hidden="true"></i> +54 (011) 155 5578 0717
						</p>
						<p>
							<?php if (ICL_LANGUAGE_CODE=='es') {
								echo "Vía mail a: info@hojasverdes.com.ar";
								} else {
								echo "info@hojasverdes.com.ar";
								} ?>
						</p>
						<p>
							<?php if (ICL_LANGUAGE_CODE=='es') {
								echo "	O visite nuestro Centro de Distribución en
									Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
								} else {
								echo "Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
								} ?>

						</p>
						<img src="<?php echo get_template_directory_uri(); ?>/img/fabrica-hojas-verdes.png" class="img-fluid" />
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
