<!-- sidebar -->
<aside class="sidebar col-lg-3" role="complementary">

	<?php //get_template_part('searchform');?>

	<!-- ACTIVIDADES -->

	<?php if (get_field('logo')) {
    ?>
		<div class="group mb-3">
			<?php if (get_post_type(get_the_ID())=='actividad') {
        ?>
					<h3>Prestador</h3>
			<?php

    } ?>
			<?php $image = get_field('logo'); ?>
			<img src="<?php echo $image['url']; ?>" class="img-fluid img-thumbnail rounded perfil" alt="<?php echo $image['alt']; ?>"/>
		</div>

	<?php

} ?>
	<div class="group mb-2">

	<h3>Consultas y reservas</h3>
	<?php if (get_field('direccion')) {
    ?>
		<p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php the_field('direccion'); ?></p>
	<?php

} ?>
	<?php if (get_field('telefonos')) {
    ?>
		<p><i class="fa fa-phone" aria-hidden="true"></i> <?php the_field('telefonos'); ?></p>
	<?php

} ?>
	<?php if (get_field('mail')) {
    ?>
		<p><i class="fa fa-at" aria-hidden="true"></i> <?php the_field('mail'); ?></p>
	<?php

} ?>
	<?php if (get_field('website')) {
    ?>
		<p><i class="fa fa-globe" aria-hidden="true"></i> <?php the_field('website'); ?></p>
	<?php

} ?>
	<?php if (get_field('facebook')) {
    ?>
		<p><i class="fa fa-facebook-official" aria-hidden="true"></i> <?php the_field('facebook'); ?></p>
	<?php

} ?>
	<?php if (get_field('twitter')) {
    ?>
		<p><i class="fa fa-twitter-square" aria-hidden="true"></i> <?php the_field('twitter'); ?></p>
	<?php

} ?>
	<?php if (get_field('instagram')) {
    ?>
		<p><i class="fa fa-instagram" aria-hidden="true"></i> <?php the_field('instagram'); ?></p>
	<?php

} ?>
	<?php if (get_field('coordenadas_gps')) {
    ?>
		<p><i class="fa fa-compass" aria-hidden="true"></i> <?php the_field('coordenadas_gps'); ?></p>
	<?php

} ?>
	<?php if (get_field('booking')) {
    ?>
		<a href="<?php echo get_field('booking'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/booking.png"></a>
	<?php

} ?>
	<?php if (get_field('tripadvisor')) {
    ?>
		<a href="<?php echo get_field('tripadvisor'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/tripadvisor.png"></a>
	<?php

} ?>

</div>

	<div class="sidebar-widget">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-1')) {
    ;
} ?>
	</div>

	<div class="sidebar-widget">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) {
    ;
} ?>
	</div>
</aside>
<!-- /sidebar -->
