<?php
    $i=0;
    if (have_posts()): while (have_posts()) : the_post();
        if (!is_user_logged_in()) {
            get_template_part('conts/content', 'product');
        }else{
           get_template_part('conts/content', 'product-logged');
        }
        $i++;
    endwhile; ?>
<?php else: ?>
	<!-- article -->
	<article>
		<h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>
	</article>
	<!-- /article -->
<?php endif; ?>
