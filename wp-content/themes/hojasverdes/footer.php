			<!-- footer -->
			<footer class="footer pt-4 pb-5 bg-primary" role="contentinfo">
				<div class="container pb-4">
					  <div class="row">
							  <div class="col-12 col-sm-6 pb-2">
									  <img src="<?php echo get_template_directory_uri(); ?>/img/logo-hojas-verdes-footer.png" alt="Hojas Verdes - especias, condimentos, semillas"/>
								</div>
								<div class="col-12 col-sm-6 text-white datos mt-sm-2">
									 <ul class="float-md-right nav flex-column">
										   <li class="nav-item py-0 my-0">
												 <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-3075
											 </li>
											 <li class="nav-item py-0 my-0">
												<i class="fa fa-whatsapp" aria-hidden="true"></i> +54 (011) 155 5578 0717
											</li>
											 <li class="nav-item py-0 my-0">
												 <i class="fa fa-envelope-o" aria-hidden="true"></i> info@hojasverdes.com.ar
											</li>
											<li class="nav-item py-0 my-0">
												<i class="fa fa-home" aria-hidden="true"></i> Av. General Paz 7771, Bs. As.
										 </li>
									 </ul>
							 </div>
						</div>
				</div>
				<div class="container hidden-lg-up">
					<div class="row">
						<div class="col-12">
							<?php do_action('wpml_add_language_selector'); ?>
						</div>
					</div>
				</div>
				<div class="container hidden-md-down">
					<div class="row">
						<div class="col-6 col-sm">
							<h4>
								<?php

if (ICL_LANGUAGE_CODE == 'es')
{
    echo "ESPECIAS";
		$cat='especias';
		$cat_base='categoria';
}
else
{
    echo "SPICES";
		$cat='spices';
		$cat_base='category';
} ?>
								</h4>
								<nav class="nav flex-column">
									<?php
	$args = array(
	    'post_type' => 'product',
	    'posts_per_page' => 8,
			'meta_key'		=> 'oculto',
			'meta_value'	=> '0',
	    'tax_query' => array(
	        array(
	            'taxonomy' => 'product_cat',
	            'field' => 'slug',
	            'terms' => $cat
	        )
	    )
	);
	$ids = array();
	$posts = new WP_Query($args);

	if ($posts->have_posts()):

	    while ($posts->have_posts()):
	        $posts->the_post(); ?>
														<a class="nav-link" href="<?php the_permalink(); ?>">  <?php if (ICL_LANGUAGE_CODE=='es') {
								                  the_title();
								              } else {
								                  the_field('nombre_en');
								              } ?></a>
								<?php
	    endwhile;
	endif;
	?>
												<a class="nav-link" href="<?php echo home_url(); ?>/<?php echo $cat_base; ?>/<?php echo $cat; ?>"><?php if (ICL_LANGUAGE_CODE=='es') {
															echo 'VER MÁS';
													} else {
														echo 'SEE MORE';
													} ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</nav>
						</div>
						<div class="col-md-6 col-lg">
							<h4>
								<?php

if (ICL_LANGUAGE_CODE == 'es')
{
    echo "CONDIMENTOS";
		$cat='condimentos';
}
else
{
    echo "CONDIMENTS";
		$cat='condiments';
} ?>
							</h4>
							<nav class="nav flex-column">
								<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 8,
		'meta_key'		=> 'oculto',
		'meta_value'	=> '0',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $cat
        )
    )
);
$ids = array();
$posts = new WP_Query($args);

if ($posts->have_posts()):

    while ($posts->have_posts()):
        $posts->the_post(); ?>
													<a class="nav-link" href="<?php the_permalink(); ?>">  <?php if (ICL_LANGUAGE_CODE=='es') {
							                  the_title();
							              } else {
							                  the_field('nombre_en');
							              } ?></a>
							<?php
    endwhile;
endif;
?>
<a class="nav-link" href="<?php echo home_url(); ?>/<?php echo $cat_base; ?>/<?php echo $cat; ?>"><?php if (ICL_LANGUAGE_CODE=='es') {
			echo 'VER MÁS';
	} else {
		echo 'SEE MORE';
	} ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
							</nav>
						</div>
						<div class="col-md-6 col-lg">
							<h4><?php

if (ICL_LANGUAGE_CODE == 'es')
{
    echo "DESHIDRATADOS";
		$cat='deshidratados';
}
else
{
    echo "DEHYDRATED";
		$cat='dehydrated';

} ?></h4>
<nav class="nav flex-column">
	<?php
$args = array(
'post_type' => 'product',
'posts_per_page' => 8,
'meta_key'		=> 'oculto',
'meta_value'	=> '0',
'tax_query' => array(
array(
'taxonomy' => 'product_cat',
'field' => 'slug',
'terms' => $cat
)
)
);
$ids = array();
$posts = new WP_Query($args);

if ($posts->have_posts()):

while ($posts->have_posts()):
$posts->the_post(); ?>
						<a class="nav-link" href="<?php the_permalink(); ?>">  <?php if (ICL_LANGUAGE_CODE=='es') {
                  the_title();
              } else {
                  the_field('nombre_en');
              } ?></a>
<?php
endwhile;
endif;
?>
<a class="nav-link" href="<?php echo home_url(); ?>/<?php echo $cat_base; ?>/<?php echo $cat; ?>"><?php if (ICL_LANGUAGE_CODE=='es') {
			echo 'VER MÁS';
	} else {
		echo 'SEE MORE';
	} ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
</nav>
						</div>
						<div class="col-md-6 col-lg">
							<h4>
								<?php

if (ICL_LANGUAGE_CODE == 'es')
{
    echo "FRUTAS SECAS";
		$cat='frutos-secos';
}
else
{
    echo "DRY FRUITS";
		$cat='dry-fruits';
} ?>
								</h4>
								<nav class="nav flex-column">
									<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 8,
		'meta_key'		=> 'oculto',
		'meta_value'	=> '0',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $cat
        )
    )
);
$ids = array();
$posts = new WP_Query($args);

if ($posts->have_posts()):

    while ($posts->have_posts()):
        $posts->the_post(); ?>
														<a class="nav-link" href="<?php the_permalink(); ?>">  <?php if (ICL_LANGUAGE_CODE=='es') {
								                  the_title();
								              } else {
								                  the_field('nombre_en');
								              } ?></a>
								<?php
    endwhile;
endif;
?>
<a class="nav-link" href="<?php echo home_url(); ?>/<?php echo $cat_base; ?>/<?php echo $cat; ?>"><?php if (ICL_LANGUAGE_CODE=='es') {
			echo 'VER MÁS';
	} else {
		echo 'SEE MORE';
	} ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</nav>
						</div>
						<div class="col-md-6 col-lg">
							<h4><?php

if (ICL_LANGUAGE_CODE == 'es')
{
    echo "SEMILLAS";
		$cat='semillas';
}
else
{
    echo "SEEDS";
		$cat='seeds';

} ?></h4>
<nav class="nav flex-column">
	<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 8,
		'meta_key'		=> 'oculto',
		'meta_value'	=> '0',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $cat
        )
    )
);
$ids = array();
$posts = new WP_Query($args);

if ($posts->have_posts()):

    while ($posts->have_posts()):
        $posts->the_post(); ?>
						<a class="nav-link" href="<?php the_permalink(); ?>">  <?php if (ICL_LANGUAGE_CODE=='es') {
                  the_title();
              } else {
                  the_field('nombre_en');
              } ?></a>
<?php
    endwhile;
endif;
?>
				<a class="nav-link" href="<?php echo home_url(); ?>/categoria/semillas">
					<?php if (ICL_LANGUAGE_CODE=='es') {
								echo 'VER MÁS';
						} else {
							echo 'SEE MORE';
						} ?>
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
</nav>
						</div>
					</div>
				</div>
			</footer>
			<!-- /footer -->
			<!--</div>-->
			<!-- /row -->
		<!--</div>-->
		<!-- /container -->

		<?php wp_footer(); ?>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
