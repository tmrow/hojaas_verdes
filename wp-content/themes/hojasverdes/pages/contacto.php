<?php
/**
 * Template Name: Contacto
 * @package
 */
?><?php get_header(); ?>

<main role="main" class="container-full">
  <section id="main-image" class="bg-primary mid">
    <div class="image-cover">
      <?php
      echo wp_get_attachment_image(367, 'full', true, array( "class" => "img-full" ));
       ?>    </div>

    <div class="caption">
        <h1>
          <?php if (ICL_LANGUAGE_CODE=='es') { echo 'CONTACTO'; }else{ echo 'CONTACT'; } ?>
      	</h1>
        <h3>
          <small class="text-lowercase">
            <?php if (ICL_LANGUAGE_CODE=='es') { echo 'Comunicate con nosotros'; }else{ echo ''; } ?>
          </small>
        </h3>
    </div>
  </section>
  	<div class="container py-lg-5 py-xl-5">
  		<div class="row">
  			<section id="main-content" class="col mb-4">
          <div class="container solapa">
            <div class="row">

                  <section class="form col-12 col-md-7">

                                  <form class="m-x-auto" id="contact-form" data-abide='ajax' data-toggle="validator" role="form">
              	                    <h5 class="text-center m-b-2"></h5>
                                    <input type="hidden" class="form-control" id="paquete" value="<?php echo get_the_title($id); ?>">

                                      <fieldset>
                                        <div class="form-group py-1">

                                       <input type="text" data-error="" class="form-control" id="name" placeholder="<?php if (ICL_LANGUAGE_CODE=='es') { echo 'Nombre'; }else{ echo 'Name'; } ?>" required>
                                       <div class="help-block with-errors"></div>
                                     </div>
                                     <div class="form-group py-1">
                                       <input type="phone"  data-error="" class="form-control" id="phone" placeholder="<?php if (ICL_LANGUAGE_CODE=='es') { echo 'Teléfono'; }else{ echo 'Phone'; } ?>" required>
                                       <div class="help-block with-errors"></div>
                                     </div>
                                       <div class="form-group py-1">
                                         <input type="email" class="form-control" id="email" placeholder="Email" data-error="<?php if (ICL_LANGUAGE_CODE=='es') { echo 'El email no es correcto'; }else{ echo 'Wrong mail'; } ?>" required>
                                       <div class="help-block with-errors"></div>
                                   </div>
                                  <div class="form-group py-1">
              				                  <textarea class="form-control w-100" rows="5" id="mensaje" data-error="" placeholder="<?php if (ICL_LANGUAGE_CODE=='es') { echo 'Comentarios'; }else{ echo 'Comments'; } ?>"></textarea>
                                        <div class="help-block with-errors"></div>
              				            </div>
                                <div class="form-group py-1">
                                  <div class="g-recaptcha" id="capcha" data-sitekey="6LepuyQUAAAAAK7yLFIJ67lH-vAXwQd-ByjZ5Rfe"></div>
                                </div>

                                  <div class="alert alert-success">
                                      <?php if (ICL_LANGUAGE_CODE=='es') { echo 'Mensaje enviado.'; }else{ echo 'Message sended.'; } ?>
                                  </div>
                                  <div class="alert alert-danger capcha">
                                    <?php if (ICL_LANGUAGE_CODE=='es') { echo 'El Capcha no es correcto. Intentalo nuevamente.'; }else{ echo 'Wrong capcha. Try again.'; } ?>

                                  </div>
                                  <div class="alert alert-danger not-send">
                                    <?php if (ICL_LANGUAGE_CODE=='es') { echo 'No se pudo enviar el mensaje.'; }else{ echo 'Message not send'; } ?>

                                  </div>



                                          <div class="form-group mt-2">
                                              <div class="text-right">
                                                  <button type="submit" id="submit"  name="submit" class="btn btn-primary btn-lg noajax" aria-label=""><?php  if (ICL_LANGUAGE_CODE=='es') { echo 'ENVIAR'; }else{ echo 'SEND'; } ?>
                                                  </button>
                                              </div>
                                          </div>
                                      </fieldset>
                                  </form>

                  </section>
                  <section class="contact col-12 col-md-5 mt-3 txt-light-grey">
                    <div class="px-2">
                      <p>
                        <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-3075  <br /><i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4657-5972
                         <br /> <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-0539
                      </p>
                      <p>
                        <i class="fa fa-whatsapp" aria-hidden="true"></i> +54 (011) 155 5578 0717
                      </p>
                      <p>
                        <?php if (ICL_LANGUAGE_CODE=='es') {
                          echo "Vía mail a: info@hojasverdes.com.ar";
                          } else {
                          echo "info@hojasverdes.com.ar";
                          } ?>
                      </p>
                      <p>
                        <?php if (ICL_LANGUAGE_CODE=='es') {
                          echo "	O visite nuestro Centro de Distribución en
                            Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
                          } else {
                          echo "Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
                          } ?>

                      </p>
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.158741358089!2d-58.53312948476968!3d-34.62542858045383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcc822029185ef%3A0x65906368a1680709!2sAv.+Gral.+Paz+7771%2C+Ciudadela%2C+Buenos+Aires!5e0!3m2!1ses-419!2sar!4v1498825133590" width="100%" height="300" frameborder="0" style="border:0; margin-top:10px;" allowfullscreen></iframe>
                    </div>
                	</section>
              </div>
          </div>
  			</section>
  		</div>
  </div>
</main>







    <?php get_footer(); ?>
