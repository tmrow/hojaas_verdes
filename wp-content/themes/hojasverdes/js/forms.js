jQuery(document).ready(function($) {
var $home_url= 'http://hojasverdes.com.ar/new/wp-content/themes/hojasverdes/inc/';
function submitForm_contact() {
    $('#contact-form #submit').text('ENVIANDO...');
     var name = $('#contact-form #name').val();
     var email = $('#contact-form #email').val();
     var phone = $('#contact-form #phone').val();
     var mensaje = $('#contact-form #mensaje').val();
     var dataString = 'name='+ name + '&email=' + email + '&phone=' + phone + '&mensaje=' + mensaje;
     $.ajax({
             type: "POST",
             url: $home_url+'process.php',
              data: dataString,
             success: function (data) {
                 $('#contact-form .alert').fadeOut();
                 if (data == 'ok') {
                     $('#contact-form .alert-success').fadeIn();
                     $('#contact-form #submit').text('ENVIAR');
                 } else {
                     $('#contact-form .alert-danger.not-send').fadeIn();
                     $('#contact-form #submit').text('ENVIAR');
                 }
             }
         });
 }
  $('#contact-form').validator().on('submit', function(e) {
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            $('#contact-form .alert-danger.capcha').fadeIn();
        } else {
          if(e.isDefaultPrevented()){
            $('#contact-form .alert-danger.not-send').fadeIn();
          }else{
             e.preventDefault();
             submitForm_contact();
          }
      }
  });
});
