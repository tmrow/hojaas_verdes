function fullheight(){
  var height = jQuery(window).height();
  jQuery('.fullheight').height(height);
  jQuery('#presentation').css('margin-top', height);
}
function weightSelect(){
  jQuery('.abierto .weight-select').change(function(){
      var $small = jQuery(this).closest('.small'),
      $kg = $small.find('showkg');

    jQuery(".abierto .showkg" ).hide();

    if(jQuery(this).val() == '1'){ // or this.value == 'volvo'
     jQuery(".abierto .onekg" ).show();
    }
    if(jQuery(this).val() == '10'){ // or this.value == 'volvo'
      jQuery(".abierto .tenkg" ).show();
    }
    if(jQuery(this).val() == '25'){ // or this.value == 'volvo'
      jQuery(".abierto .twentykg" ).show();
    }
     if(jQuery(this).val() == '50'){ // or this.value == 'volvo'
      jQuery(".abierto .fiftykg" ).show();
    }
  });
}

function toggleProducts(){
  jQuery('.product .tr').click(function(event) {
    jQuery('.product .tr').removeClass('abierto');
      jQuery(this).addClass("abierto");
      jQuery(this).closest('.close-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
      weightSelect();
  });
}



jQuery(document).ready(function($) {


  


    toggleProducts();
      $.fn.almComplete = function(alm){
        console.log("Ajax Load More Complete!");
        toggleProducts();
      };



    if ($(window).width()>768){
      fullheight();
    }

    if ($(window).width() < 992){
    if ($(".carousel-tax")[0]){
      $('.carousel-tax').owlCarousel({
        items:6,
        margin:0,
        loop: false,
        autoplay:false,
        dots: false,
        autoHeight: false,
        autoWidth: true,
        merge: false,
        nav:false,
      });
    }
  }
});
jQuery(window).resize(function() {
  if (jQuery(window).width()>768){
    fullheight();
  }
});
