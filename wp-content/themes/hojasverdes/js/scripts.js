(function ($, root, undefined) {

	$(function () {

		'use strict';

		// DOM ready, take it away

	});

})(jQuery, this);

function fullheight(){
  var height = jQuery(window).height();
  jQuery('.fullheight').height(height);
  jQuery('#presentation').css('margin-top', height);
}


jQuery(document).ready(function($){

	$(".bulto-cerrado .qty").keydown(function(e) {
			if (e.keyCode > 36 && e.keyCode < 41){
				return true;
			} else {
				return false;
			}
  });

	$('#ex1').zoom();


		if ($(window).width()>768){
			fullheight();
		}
	  $("form.cart").on("change", "input.qty", function() {
	          if (this.value === "0")
	              this.value = "1";

	          $(this.form).find(".add_to_cart_button").data("quantity", this.value);
	      });

	      /* remove old "view cart" text, only need latest one thanks! */
	      $(document.body).on("adding_to_cart", function() {
	          $("a.added_to_cart").remove();
	      });





	var wind = $(window).width();
	var container = $("#sizer").width();
	var padding = wind-container;

	$('.caption.full, .featured .caption').css('padding-left', padding/2);


	if ($(".owl-carousel")[0]){
		$('.owl-carousel').owlCarousel({
			items:1,
			margin:0,
			loop: true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			dots: false,
			autoHeight: false,
			autoWidth: true,
			merge: true,
			nav:true,
		});
	}

	if ($(".home-carousel")[0]){
		$('.home-carousel').owlCarousel({
			items:1,
			margin:0,
			loop: false,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			dots: false,
			autoHeight: true,
			autoWidth: false,
			merge: true,
			nav:true,
		});
	}

	$('.tab').click(function(event) {
		$('.tab, .tab-container').removeClass('active');
		$(this).addClass('active');
		var tabContent = $(this).data('tab');
		$(tabContent).addClass('active');
		$(tabContent + '.tab-boxes').addClass('active');
		if ($(window).width()>768){
			$('html, body').animate({scrollTop:$(tabContent+'.active').prop("scrollHeight")-250}, 'slow');
		}else{
			$('html, body').animate({scrollTop:$(tabContent+'.active').position().top+380}, 'slow');
		}
		$(tabContent+'.active').focus();
	});

	$('.page .btn.map, .archive .btn.map').click(function(event) {
		$(this).closest('.tax-head').find('.list').removeClass('active');
		$(this).addClass('active');
		$(this).closest('.tax-tabs').find('.tab-container .list').removeClass('active');
		$(this).closest('.tax-tabs').find('.tab-container .map').addClass('active');
	});

	$('.page .btn.list, .archive .btn.list').click(function(event) {
		$(this).closest('.tax-head').find('.map').removeClass('active');
		$(this).addClass('active');
		$(this).closest('.tax-tabs').find('.tab-container .list').addClass('active');
		$(this).closest('.tax-tabs').find('.tab-container .map').removeClass('active');
	});
	$('.home .btn.map').click(function(event) {
		$('.list').removeClass('active');
		$('.map').addClass('active');
	});

	$('.home .btn.list').click(function(event) {
		$('.list').addClass('active');
		$('.map').removeClass('active');
	});
	$(window).scroll(function(){
	    var scroll = $(window).scrollTop();
	    if (scroll >= 40) {
				$('#main-navbar').addClass('scrolltop');
	    }else{
				$('#main-navbar').removeClass('scrolltop');
			}

	});

});
jQuery(document).on('click', '.yamm .dropdown-menu', function(e) {
  //e.stopPropagation();
})
jQuery(window).resize(function() {
  if (jQuery(window).width()>768){
    fullheight();
  }
});
