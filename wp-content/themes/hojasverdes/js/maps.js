(function($) {
	function initMap($el, $route) {
	  var bounds = [];
	  var waypts = [];
	  var $markers = $($el).find('.marker');

	  var map = new GMaps({
	    div: $el,
	    scrollwheel: false,
	    zoomControl : false,
	        disableDefaultUI: true, // a way to quickly hide all controls

	    zoomControlOpt: {
	      style: 'LARGE',
	      position: 'TOP_RIGHT'
	    },
	    panControlOpt: {
	      position: 'TOP_RIGHT'
	    }
	  });
	$markers.each(function() {
	  // DEFINE CURRENT/NEXT MARKER
	  $this = $(this);
	  $next = $(this).next();

	  // DEFINE BOUNDS
	  var latlng = new google.maps.LatLng($(this).data('lat'), $(this).data('lng'));
	  bounds.push(latlng);

	    waypts.push({
	      location: latlng,
	      stopover: true
	    });
	  map.addMarker({
	    lat: $(this).data('lat'),
	    lng: $(this).data('lng'),
	    icon: $(this).data('icon'),
	    infoWindow: {
	      content: $(this).html()
	    }

	  });
	});
	if ($route==true){
		map.drawRoute({
		  origin: [$markers.first().data('lat'), $markers.first().data('lng')],
		  destination: [$markers.last().data('lat'), $markers.last().data('lng')],
		  waypoints: waypts,
		  travelMode: 'driving',
		  strokeColor: 'orange',
		  strokeOpacity: 1,
		  strokeWeight: 4
		});
	}
	map.addStyle({
	  styledMapName:"Styled Map",
	  styles: [
    {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "hue": "#ff0000"
            }
        ]
    }
],
	  mapTypeId: "map_style"
	});
	map.setStyle("map_style");
	map.fitLatLngBounds(bounds);
	}

$(document).ready(function(){

	$('.acf-map').each(function(){
		if ($(this).hasClass('route')) {
			initMap(this, true);
		} else {
			initMap(this, false);
		}
	});
});

})(jQuery);
