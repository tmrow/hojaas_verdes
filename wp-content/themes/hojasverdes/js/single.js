jQuery(document).ready(function($){

  //carousel

  $('.owl-carousel').owlCarousel({
		items:1,
		margin:0,
		loop: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		dots: false,
		autoHeight: false,
		autoWidth: true,
		merge: true,
		nav:true,
	});


});
