<?php
/*
 *  Author: Tomorrow studio
 *  URL: tomorrow.com.ar
 */
$theme_name='theme';




 // Load HTML5 Blank styles
 function html5blank_styles()
 {
     wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.min.css', array(), '1.0', 'all');
     wp_enqueue_style('normalize'); // Enqueue it!

     wp_register_style('owl-slider', get_template_directory_uri() . '/css/inc/owl.carousel.min.css', array(), '', 'all');
     wp_enqueue_style('owl-slider'); // Enqueue it!

     wp_register_style('Main', get_template_directory_uri() . '/css/main.css', array(), '', 'all');
     wp_enqueue_style('Main'); // Enqueue it!
 }

 // Load HTML5 Blank scripts (header.php)
 function html5blank_header_scripts()
 {
     if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
         wp_register_script('owl-carousel', get_template_directory_uri() . '/js/plugins/owl.carousel.min.js', array('jquery'), true); // Custom scripts
       wp_enqueue_script('owl-carousel'); // Enqueue it!

         wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
         wp_enqueue_script('conditionizr'); // Enqueue it!

         wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
         wp_enqueue_script('modernizr'); // Enqueue it!

         wp_register_script('font-awesome', 'https://use.fontawesome.com/26413d75b0.js', array('jquery'), false); // Custom scripts
         wp_enqueue_script('font-awesome'); // Enqueue it!

         wp_register_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js', array('jquery'), false); // Custom scripts
         wp_enqueue_script('bootstrap-js'); // Enqueue it!

         wp_register_script('waypoints', get_template_directory_uri() . '/js/plugins/jquery.waypoints.min.js', array('jquery'), '', true); // Custom scripts
         wp_enqueue_script('waypoints'); // Enqueue it!

         wp_register_script('zoom', get_template_directory_uri() . '/js/plugins/jquery.zoom.min.js', array('jquery'), '', true); // Custom scripts
         wp_enqueue_script('zoom'); // Enqueue it!

         wp_register_script('single-scripts', get_template_directory_uri() . '/js/scripts.min.js', array('jquery'), '', true); // Custom scripts
         wp_enqueue_script('single-scripts'); // Enqueue it!

     }
     if (is_admin()){
         wp_enqueue_script('custom_admin_script', get_bloginfo('template_url').'/js/admin_script.js', array('jquery'));
       }
 }

 // Load HTML5 Blank conditional scripts
 function html5blank_conditional_scripts()
 {

     if (is_archive() or is_search()) {
       wp_register_script('main', get_template_directory_uri() . '/js/main.min.js', array('jquery'), '1.0.0', true); // Conditional script(s)
       wp_enqueue_script('main'); // Enqueue it!
     }

    if (is_page('contacto')) {

      wp_register_script('forms', get_template_directory_uri() . '/js/forms.min.js', array('jquery'), '1.0.0', true); // Conditional script(s)
      wp_enqueue_script('forms'); // Enqueue it!

      wp_register_script('validator', get_template_directory_uri() . '/js/plugins/validator.min.js', array('jquery'), '1.0.0', true); // Conditional script(s)
      wp_enqueue_script('validator'); // Enqueue it!
    }
}

 function my_acf_init()
 {
     acf_update_setting('google_api_key', 'AIzaSyCTohzSpCGAnMEWEnFaE0ZQKZikJ0U8N84');
 }

 add_action('acf/init', 'my_acf_init');





/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/
function create_bootstrap_menu($theme_location)
{
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu = get_term($locations[$theme_location], 'nav_menu');
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list = '<ul class="main-menu nav navbar-nav">' ."\n";

        foreach ($menu_items as $menu_item) {
            if ($menu_item->menu_item_parent == 0) {
                $parent = $menu_item->ID;

                $menu_array = array();
                foreach ($menu_items as $submenu) {
                    if ($submenu->menu_item_parent == $parent) {
                        $bool = true;
                        $menu_array[] = '<li><a href="' . $submenu->url . '">' . $submenu->title . '</a></li>' ."\n";
                    }
                }
                if ($bool == true && count($menu_array) > 0) {
                    $menu_list .= '<li class="dropdown yamm-fw">' ."\n";
                    $menu_list .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu_item->menu_class . ' <span class="caret"></span></a>' ."\n";

                    $menu_list .= '<ul class="dropdown-menu">' ."\n";
                    $menu_list .= implode("\n", $menu_array);
                    $menu_list .= '</ul>' ."\n";
                } else {
                    $menu_list .= '<li>' ."\n";
                    $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";
                }
            }

            // end <li>
            $menu_list .= '</li>' ."\n";
        }

        $menu_list .= '</ul>' ."\n";
    } else {
        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
    }

    echo $menu_list;
}





// custom excerpt
    function custom_excerpt_more($more)
    {
        return '...';
    }
    add_filter('excerpt_more', 'custom_excerpt_more');

// custom login logo
function custom_login_logo()
{
    echo '<style type="text/css">
	body.login {

	}
	#login h1 a {
	 background-image: url(http://tomorrow.com.ar/mails/Mail-02.png);
	 width:150px !important;
	 background-size:150px !important;
	 height:30px;
	}
</style>';
}
add_action('login_head', 'custom_login_logo');

function custom_admin_logo()
{
    echo '
        <style type="text/css">
            #wp-admin-bar-wp-logo {
		width:80px !important;
		background-image: url(http://tomorrow.com.ar/mails/Mail-02.png) !important;
		background-size:90px !important;
		background-repeat:no-repeat !important;
		background-position:center center !important;
		height:32px !important;
		padding:0 20px !important;
	}
	 #wp-admin-bar-wp-logo.menupop.hover {

			background: none !important;
			background-size:80px !important;
			clear:inerith !important;
	}

	#wp-admin-bar-wp-logo .ab-item {
		display:none !important;
	}
        </style>
    ';
}
add_action('admin_head', 'custom_admin_logo');


function fontawesome_dashboard()
{
    wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
}

add_action('admin_init', 'fontawesome_dashboard');

function fontawesome_icon_dashboard()
{
    echo "<style type='text/css' media='screen'>

   #adminmenu #menu-posts div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f1ea';
       font-size:18px;
     }
     /*#woocommerce-product-data {
       display:none;
     }*/
     #adminmenu #menu-posts-actividad div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f030';
       font-size:18px;
     }


    #adminmenu #menu-posts-alojamiento div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f0fd';
       font-size:18px;
     }

	  #adminmenu #menu-posts-gastronomia div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f0f5';
       font-size:18px;
     }

	  #adminmenu #menu-posts-mapa div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f279';
       font-size:18px;

     }

	   #adminmenu #menu-posts-evento div.wp-menu-image:before {
       font-family: Fontawesome !important;
       content: '\\f073';
       font-size:18px;

     }



     #adminmenu #menu-posts-investigacion {
     margin-bottom:20px !important;
 	}
	#adminmenu #menu-posts-calendario {
     margin-bottom:20px !important;
 	}
	#adminmenu .toplevel_page_wpcf7 {
	}
    #adminmenu #menu-posts-institucional div.wp-menu-image:before {
        font-family: Fontawesome !important;
        content: '\\f19c';
       font-size:16px !important;
       margin-top:2px !important;
     }
     #adminmenu #menu-posts-staff div.wp-menu-image:before {
        font-family: Fontawesome !important;
        content: '\\f0c0';
       font-size:16px !important;
       margin-top:2px !important;
     }
     #menu-comments {
        display:none !important;
        }
	       #toplevel_page_acf-options {
        display:none !important;
        }
        .inline-edit-group.wp-clearfix {
          display:none;
        }
         #woocommerce-fields.inline-edit-col{
           display:none;
         }
        .wc-order-data-row .button.add-order-shipping, .wc-order-data-row .button.add-order-fee {
          display:none;
        }

        ";
    if (!(current_user_can('administrator'))) {
        echo "
        #toplevel_page_edit-post_type-acf-field-group, #menu-settings {display:none;}
        #toplevel_page_jetpack, #menu-posts, #menu-pages, #menu-tools {
          display:none;
        }
        .general_options, .inventory_options, .shipping_options, .linked_product_options, .attribute_options, .variations_options, .advanced_options {
          display:none !important;
        }
        #dynamic_pricing_data .section {
          display:none;
        }
        #toplevel_page_woocommerce .wp-submenu li:nth-child(4), #toplevel_page_woocommerce .wp-submenu li:nth-child(5), #toplevel_page_woocommerce .wp-submenu li:nth-child(6), #toplevel_page_woocommerce .wp-submenu li:nth-child(7),
        #toplevel_page_woocommerce .wp-submenu li:nth-child(8), #toplevel_page_woocommerce .wp-submenu li:nth-child(9){
          display:none;
        }
         ";
    }
    echo "
     </style>"



     ;
}
add_action('admin_head', 'fontawesome_icon_dashboard');





/*------------------------------------*\
    Theme Support
\*------------------------------------*/


if (!isset($content_width)) {
    $content_width = 900;
}

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('lg', 1920, 600);

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
    Functions
\*------------------------------------*/


// Register custom navigation walker
    require_once('inc/wp-bootstrap-navwalker.php');

    register_nav_menus(array(
            'primary' => __('Primary Menu', $theme_name),
    ));


// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
}
require_once('inc/wp_bootstrap_pagination.php');

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() and comments_open() and (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ('div' == $args['style']) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    } ?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ('div' != $args['style']) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) {
        echo get_avatar($comment, $args['180']);
    } ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>">
		<?php
            printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'), '  ', ''); ?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ('div' != $args['style']) : ?>
	</div>
	<?php endif; ?>
<?php

}

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
//add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
//add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
//add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

//template url
function template_url()
{
    $url=get_template_directory_uri();
    echo $url;
}
add_action('init', 'my_custom_init');

function my_custom_init()
{
    add_post_type_support('product', 'publicize');
}

// REGISTER POST type
//ACTIVIDADES
/*$post_type_name='actividad';
$singular_name='Actividad';
$plural_name='Actividades';

$labels = array(
    'name'=>$plural_name,
    'singular_name'=>$singular_name,
    'add_new'=>'Agregar '. $singular_name,
    'add_new_item'=>'Crear '.$singular_name,
    'edit_item'=>'Editar'. $singular_name,
    'new_item'=>'Nuevo '. $singular_name,
    'view_item'=>'Ver '. $singular_name,
    'search_items'=>'Buscar '. $singular_name,
);

$args = array(
    'labels'=>$labels,
    'public'=>true,
    'menu_position'=>4,
        // Uncomment the following line to change the slug;
        // 'rewrite' => array( 'slug' => 'portfolio'),
    'supports'=>array('title','editor','thumbnail', 'excerpt'),
    'query_var' => true,
    'show_in_nav_menus' => true,
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug' => $post_type_name),
    'taxonomies' => array('post_tag')
);

register_post_type($post_type_name, $args);*/


// REGISTER TAXONOMIES
// ACTIVIDADES
/*$tax_name='actividades-tax';
$tax_singular_name='Categoría';
$tax_plural_name='Categorías';

function register_tax_taxonomy()
{
    $tax_labels = array(
        'name' => 'Categorías de actividades',
        'singular_name' => 'Categoría',
        'search_items' =>  'Buscar',
        'popular_items' => '',
        'all_items' => 'Toda',
        'parent_item' => 'Categoría parent',
        'parent_item_colon' => 'Categoría parent:',
        'edit_item' => 'Editar categoría',
        'update_item' => 'Actualizar categoría',
        'add_new_item' => 'Agregar categoría',
        'new_item_name' => 'Nuevo nombre de categoría',
        'separate_items_with_commas' => 'Separar categorías con comas',
        'add_or_remove_items' => 'Agregar o borrar categoría',
        'choose_from_most_used' => 'Elegir entre las categorías más usadas',
        'menu_name' => 'Categorías'
    );

    register_taxonomy('actividades',
    array('actividad'),
    array(
        'hierarchical'=>true,
        'show_ui'=>true,
        'labels' => $tax_labels,
        'public' => true,
        'query_var'=> true,
        'label' => 'Taxonomias',
        'rewrite' => array('slug' => 'actividades', 'hierarchical' => true),
        'taxonomies' => array('post_tag')
    )
);

    $tax_labels = array(
  'name' => 'Categorías de alojamientos',
  'singular_name' => 'Categoría',
  'search_items' =>  'Buscar',
  'popular_items' => '',
  'all_items' => 'Toda',
  'parent_item' => 'Categoría parent',
  'parent_item_colon' => 'Categoría parent:',
  'edit_item' => 'Editar categoría',
  'update_item' => 'Actualizar categoría',
  'add_new_item' => 'Agregar categoría',
  'new_item_name' => 'Nuevo nombre de categoría',
  'separate_items_with_commas' => 'Separar categorías con comas',
  'add_or_remove_items' => 'Agregar o borrar categoría',
  'choose_from_most_used' => 'Elegir entre las categorías más usadas',
  'menu_name' => 'Categorías'
);

    register_taxonomy('alojamientos',
array('alojamiento'),
array(
  'hierarchical'=>true,
  'show_ui'=>true,
  'labels' => $tax_labels,
  'public' => true,
  'query_var'=> true,
  'label' => 'Taxonomias',
  'rewrite' => array('slug' => 'alojamientos', 'hierarchical' => true),
  'taxonomies' => array('post_tag')
)
);

    $tax_labels = array(
  'name' => 'Categorías de gastronomía',
  'singular_name' => 'Categoría',
  'search_items' =>  'Buscar',
  'popular_items' => '',
  'all_items' => 'Toda',
  'parent_item' => 'Categoría parent',
  'parent_item_colon' => 'Categoría parent:',
  'edit_item' => 'Editar categoría',
  'update_item' => 'Actualizar categoría',
  'add_new_item' => 'Agregar categoría',
  'new_item_name' => 'Nuevo nombre de categoría',
  'separate_items_with_commas' => 'Separar categorías con comas',
  'add_or_remove_items' => 'Agregar o borrar categoría',
  'choose_from_most_used' => 'Elegir entre las categorías más usadas',
  'menu_name' => 'Categorías'
);

    register_taxonomy('restaurantes',
array('gastronomia'),
array(
  'hierarchical'=>true,
  'show_ui'=>true,
  'labels' => $tax_labels,
  'public' => true,
  'query_var'=> true,
  'label' => 'Taxonomias',
  'rewrite' => array('slug' => 'restaurantes', 'hierarchical' => true),
  'taxonomies' => array('post_tag')
)
);
    $tax_labels = array(
  'name' => 'Categorías de eventos',
  'singular_name' => 'Categoría',
  'search_items' =>  'Buscar',
  'popular_items' => '',
  'all_items' => 'Toda',
  'parent_item' => 'Categoría parent',
  'parent_item_colon' => 'Categoría parent:',
  'edit_item' => 'Editar categoría',
  'update_item' => 'Actualizar categoría',
  'add_new_item' => 'Agregar categoría',
  'new_item_name' => 'Nuevo nombre de categoría',
  'separate_items_with_commas' => 'Separar categorías con comas',
  'add_or_remove_items' => 'Agregar o borrar categoría',
  'choose_from_most_used' => 'Elegir entre las categorías más usadas',
  'menu_name' => 'Categorías'
);

    register_taxonomy('eventos',
array('evento'),
array(
  'hierarchical'=>true,
  'show_ui'=>true,
  'labels' => $tax_labels,
  'public' => true,
  'query_var'=> true,
  'label' => 'Taxonomias',
  'rewrite' => array('slug' => 'eventos', 'hierarchical' => true),
  'taxonomies' => array('post_tag')
)
);
}
add_action('init', 'register_tax_taxonomy');

function alter_attr_wpse_102158($attr)
{
    remove_filter('wp_get_attachment_image_attributes', 'alter_attr_wpse_102158');
    $attr['class'] .= ' img-fluid';
    return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'alter_attr_wpse_102158');

//MAPAS
/*function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyDVzUUN_qmJQrM65EkCmMSkLhd-xDT5uyI';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');*/

/*------------------------------------*\
    ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

function hm_get_template_part($file, $template_args = array(), $cache_args = array())
{
    $template_args = wp_parse_args($template_args);
    $cache_args = wp_parse_args($cache_args);
    if ($cache_args) {
        foreach ($template_args as $key => $value) {
            if (is_scalar($value) || is_array($value)) {
                $cache_args[$key] = $value;
            } elseif (is_object($value) && method_exists($value, 'get_id')) {
                $cache_args[$key] = call_user_method('get_id', $value);
            }
        }
        if (($cache = wp_cache_get($file, serialize($cache_args))) !== false) {
            if (! empty($template_args['return'])) {
                return $cache;
            }
            echo $cache;
            return;
        }
    }
    $file_handle = $file;
    do_action('start_operation', 'hm_template_part::' . $file_handle);
    if (file_exists(get_stylesheet_directory() . '/' . $file . '.php')) {
        $file = get_stylesheet_directory() . '/' . $file . '.php';
    } elseif (file_exists(get_template_directory() . '/' . $file . '.php')) {
        $file = get_template_directory() . '/' . $file . '.php';
    }
    ob_start();
    $return = require($file);
    $data = ob_get_clean();
    do_action('end_operation', 'hm_template_part::' . $file_handle);
    if ($cache_args) {
        wp_cache_set($file, $data, serialize($cache_args), 3600);
    }
    if (! empty($template_args['return'])) {
        if ($return === false) {
            return false;
        } else {
            return $data;
        }
    }
    echo $data;
}

// woocommerce

add_action('woocommerce_price_html', 'wc_custom_price', 10, 2);
function wc_custom_price($price, $product)
{
    return sprintf(__('%s per KG', 'woocommerce'), woocommerce_price($product->get_price()));
}

/**
 * Plugin Name: WooCommerce Registration Fields
 * Description: My Custom registration fields.
 * Version: 1.0
 * Author: Antonio Crespo
 * License: GPL2
 */

/* Extra Fields in WooCommerce Registration */

/**
 * Add new register fields for WooCommerce registration.
 *
 * @return string Register fields HTML.
 */
function wooc_extra_register_fields()
{
    ?>

    <div class="form-group mb-2">
    <input type="text" class="form-control" name="billing_first_name" placeholder="<?php _e('Nombre', 'woocommerce'); ?>" id="reg_billing_first_name" value="<?php if (! empty($_POST['billing_first_name'])) {
        esc_attr_e($_POST['billing_first_name']);
    } ?>" />
    </div>

    <div class="form-group mb-2">
    <input type="text" class="form-control" name="billing_last_name" id="reg_billing_last_name" placeholder="<?php _e('Apellidos', 'woocommerce'); ?>" value="<?php if (! empty($_POST['billing_last_name'])) {
        esc_attr_e($_POST['billing_last_name']);
    } ?>" />
  </div>


  <div class="form-group mb-2">
    <input type="text" class="form-control" placeholder="<?php _e('Teléfono', 'woocommerce'); ?>" name="billing_phone" id="reg_billing_phone" value="<?php if (! empty($_POST['billing_phone'])) {
        esc_attr_e($_POST['billing_phone']);
    } ?>" />
  </div>

    <div class="form-group mb-2">
<input type="text" class="form-control" placeholder="<?php _e('Dirección', 'woocommerce'); ?> " name="billing_address_1" id="reg_billing_address_1" value="<?php if (! empty($_POST['billing_address_1'])) {
        esc_attr_e($_POST['billing_address_1']);
    } ?>" />
  </div>


    <div class="form-group mb-2">
    <input type="text" class="form-control" placeholder="<?php _e('Localidad / Ciudad', 'woocommerce'); ?>" name="billing_city" id="reg_billing_city" value="<?php if (! empty($_POST['billing_city'])) {
        esc_attr_e($_POST['billing_city']);
    } ?>" />
  </div>

    <?php

}

add_action('woocommerce_register_form_start', 'wooc_extra_register_fields');




/**
 * Validate the extra register fields.
 *
 * @param  string $username          Current username.
 * @param  string $email             Current email.
 * @param  object $validation_errors WP_Error object.
 *
 * @return void
 */
function wooc_validate_extra_register_fields($username, $email, $validation_errors)
{
    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
        $validation_errors->add('billing_first_name_error', __('Nombre es un campo requerido.', 'woocommerce'));
    }

    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
        $validation_errors->add('billing_last_name_error', __('Apellidos es un campo requerido.', 'woocommerce'));
    }

    if (isset($_POST['billing_razon_social']) && empty($_POST['razon_social'])) {
        $validation_errors->add('billing_razon_social_error', __('Razón social es un campo requerido.', 'woocommerce'));
    }
    if (isset($_POST['billing_iva']) && empty($_POST['iva'])) {
        $validation_errors->add('billing_iva_error', __('Es un campo requerido.', 'woocommerce'));
    }
    if (isset($_POST['billing_cuit']) && empty($_POST['cuit'])) {
        $validation_errors->add('billing_cuit_error', __('Es un campo requerido.', 'woocommerce'));
    }
    if (isset($_POST['billing_phone']) && empty($_POST['billing_phone'])) {
        $validation_errors->add('billing_phone_error', __('Teléfono es un campo requerido.', 'woocommerce'));
    }

    if (isset($_POST['billing_address_1']) && empty($_POST['billing_address_1'])) {
        $validation_errors->add('billing_address_1_error', __('Dirección es un campo requerido.', 'woocommerce'));
    }


    if (isset($_POST['billing_city']) && empty($_POST['billing_city'])) {
        $validation_errors->add('billing_city_error', __('Localidad / Ciudad es un campo requerido.', 'woocommerce'));
    }
}

add_action('woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3);

/**
 * Save the extra register fields.
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function wooc_save_extra_register_fields($customer_id)
{
    if (isset($_POST['billing_first_name'])) {
        // WordPress default first name field.
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));

        // WooCommerce billing first name.
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
    }

    if (isset($_POST['billing_last_name'])) {
        // WordPress default last name field.
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));

        // WooCommerce billing last name.
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
    }
    if (isset($_POST['billing_razon_social'])) {
        // WordPress default last name field.
        update_user_meta($customer_id, 'razon_social', sanitize_text_field($_POST['billing_razon_social']));

        // WooCommerce billing last name.
        update_user_meta($customer_id, 'billing_razon_social', sanitize_text_field($_POST['billing_razon_social']));
    }

    if (isset($_POST['billing_iva'])) {
        // WordPress default last name field.
        update_user_meta($customer_id, 'iva', sanitize_text_field($_POST['billing_iva']));

        // WooCommerce billing last name.
        update_user_meta($customer_id, 'billing_iva', sanitize_text_field($_POST['billing_iva']));
    }
    if (isset($_POST['billing_cuit'])) {
        // WordPress default last name field.
        update_user_meta($customer_id, 'cuit', sanitize_text_field($_POST['billing_cuit']));

        // WooCommerce billing last name.
        update_user_meta($customer_id, 'billing_cuit', sanitize_text_field($_POST['billing_cuit']));
    }

    if (isset($_POST['billing_phone'])) {
        // WooCommerce billing phone
        update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
    }

    if (isset($_POST['billing_address_1'])) {
        // WooCommerce billing address
        update_user_meta($customer_id, 'billing_address_1', sanitize_text_field($_POST['billing_address_1']));
    }

    if (isset($_POST['billing_postcode'])) {
        // WooCommerce billing postcode
        update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
    }

    if (isset($_POST['billing_city'])) {
        // WooCommerce billing city
        update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));
    }
}

add_action('woocommerce_created_customer', 'wooc_save_extra_register_fields');
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');



add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_address_2']);
    return $fields;
}



function woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start(); ?>
  <span class="cart-count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
  <?php

  $fragments['span.cart-count'] = ob_get_clean();

    return $fragments;
}

// CALCULAR TOTAL CON DESCUENTOS
add_filter( 'woocommerce_account_menu_items', 'custom_woocommerce_account_menu_items' );
function custom_woocommerce_account_menu_items( $items ) {
	if ( isset( $items['downloads'] ) ) unset( $items['downloads'] );
	return $items;
}

add_filter('body_class','my_class_names');
function my_class_names($classes) {
    if (! ( is_user_logged_in() ) ) {
        $classes[] = 'logged-out';
    }
    if ( is_search() ) {
      $classes[] = 'woocommerce';
    }
    return $classes;
}
function wc_ninja_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );
function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}

// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );


add_action( 'pre_get_posts', 'custom_query_vars' );
function custom_query_vars( $query ) {
  if (!is_admin() && $query->is_main_query() && !is_page()) {
    $usuario=get_user_meta(get_current_user_id());
    if (!is_user_logged_in() or $usuario['pw_user_status'][0]!='approved') {
      $query->set('meta_key', 'oculto');
      $query->set('meta_value', '0');
  }
  return $query;
}
}

//add_action( 'woocommerce_before_calculate_totals', 'woo_dinamic_price');
/*function woo_dinamic_price() {
    global $woocommerce;

    $donation = 10;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $cart_item['data']->set_price($donation);
            $cart_item['data']->set_regular_price($donation);
            //$cart_item['data']->set_subtotal($donation);
    }
}
*/

add_filter( 'woocommerce_quantity_input_args', 'jk_woocommerce_quantity_input_args', 10, 2 );

function jk_woocommerce_quantity_input_args( $args, $product ) {
  if(get_field('bulto_cerrado')){
    $precio_dinamico = array_column(get_field('_pricing_rules'), 'rules');
    $kg=$precio_dinamico[0][1]['from'];

    $args['min_value']	= $kg;	// Starting value (we only want to affect product pages, not cart)
  	$args['step'] 		= $kg;
    $args['input_value'] 	= $kg;   // Quantity steps
  }else{
    if (!is_page('cart')){
    $args['input_value'] 	= 1;	// Starting value (we only want to affect product pages, not cart)
    $args['step'] 		= 1;    // Quantity steps
    }
  }
  return $args;

}
// Variations
add_filter( 'woocommerce_available_variation', 'jk_woocommerce_available_variation' );
function jk_woocommerce_available_variation( $args ) {
$args['max_qty'] = 80; // Maximum value (variations)
$args['min_qty'] = 2; // Minimum value (variations)
return $args;
}
/**
 * Initialize Gateway Settings Form Fields
 */

?>
