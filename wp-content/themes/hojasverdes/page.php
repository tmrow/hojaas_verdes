<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<main role="main" class="container-full mt-3">
  	<div class="container py-lg-5 py-xl-5">
  		<div class="row">
  			<section id="main-content" class="col mb-4">
  				<article id="post-<?php the_ID(); ?>">
  					<?php the_content(); ?>
  				</article>
  			</section>
  		</div>
  </div>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>
