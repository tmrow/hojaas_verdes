<?php get_header(); ?>
<?php $usuario=get_user_meta(get_current_user_id()); ?>
<?php if (!is_user_logged_in() or $usuario['pw_user_status'][0]!='approved' and !is_admin()) { ?>
<main role="main" class="container-full">
  <section id="main-image" class="bg-primary mid">
    <div class="image-cover">
      <?php echo wp_get_attachment_image(445, 'full', '', array( "class" => "img-full" ));?>
    </div>
    <div class="caption">
        <h1>
          <?php if (ICL_LANGUAGE_CODE=='es') { ?>
            NUESTROS PRODUCTOS
          <?php }else{ ?>
              OUR PRODUCTS
          <?php } ?>
      	</h1>
        <h3>
          <small class="text-lowercase">
            <?php if (ICL_LANGUAGE_CODE=='es') { ?>
              Ingredientes que importan
            <?php }else{ ?>
              Ingredients that matter
            <?php } ?>
          </small>
        </h3>
    </div>
  </section>
	<section id="tax-1">
		<div class="container">
			<div class="tax-tabs" id="<?php echo $term->slug ?>">
				<div class="tax-head my-4">
				<div class="row">
          <?php get_template_part('conts/content', 'catlist'); ?>
        </div>
        <div class="clearfix"></div>
			</div>
			<div class="w-100"></div>
      <?php if($usuario['pw_user_status'][0]=='pending') { ?>
      <div class="row">
        <div class="col-12">
          <div class="alert alert-warning" role="alert">
            <strong>Tu cuenta está pendiente de activación</strong> Te avisaremos por e-mail cuando esté activa para que puedas ver los precios y hacer tus pedidos. Ante cualquier duda escribinos a ventas@hojasverdes.com.ar o llamanos al (011) 4647-3075.
          </div>
        </div>
      </div>
      <?php } ?>
      <?php if($usuario['pw_user_status'][0]=='denied') { ?>
      <div class="row">
        <div class="col-12">
          <div class="alert alert-danger" role="alert">
            <strong>Tu fue bloqueada.</strong> Ante cualquier duda escribinos a ventas@hojasverdes.com.ar o llamanos al (011) 4647-3075.
          </div>
        </div>
      </div>
      <?php } ?>
			<div class="row tab-boxes list">
        <?php wp_reset_query(); wp_reset_postdata(); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post();
         //if(get_field('oculto')!=1 and has_post_thumbnail()){
          get_template_part('conts/content', 'product');
        //}
          endwhile;
    endif; ?>
      </div>
		</div>
		</div>
		<?php get_template_part('pagination'); ?>
	</section>
</main>
<?php

} else {
    ?>
    <main role="main" class="container-full">
      <section id="main-image" class="bg-primary mid">
        <div class="image-cover">
          <?php echo wp_get_attachment_image(445, 'full', '', array( "class" => "img-full" ));?>
        </div>
        <div class="caption">
            <h1>
              PEDIDOS ONLINE
          	</h1>
            <h3>
              <small class="text-lowercase">Acceso a clientes</small>
            </h3>
        </div>
      </section>
    	<section id="tax-1">
    		<div class="container">
    			<div class="tax-tabs" id="<?php echo $term->slug ?>">
    				<div class="tax-head my-4">
    				<div class="row">
              <?php get_template_part('conts/content', 'catlist'); ?>
            </div>
            <div class="clearfix"></div>
    			</div>
    			<div class="w-100"></div>
    			<div class="row tab-boxes list">
            <?php wp_reset_query(); wp_reset_postdata(); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post();
    get_template_part('conts/content', 'product-logged');
    endwhile;
    endif; ?>
          </div>
    		</div>
    		</div>
    		<?php get_template_part('pagination'); ?>
    	</section>
      <section id="aclaraciones">
    		<div class="container">
          <div class="row">
            <div class="col-12">
              <div class="alert alert-warning" role="alert">
                <p>
                  *  IVA del 10.5 %
                </p>
                <p>
                  ** IVA del 21 %
                </p>
                <p>
                  Los precios no incluyen IVA y pueden ser modificados sin previo
                  aviso.
                </p>
        <p>
          Los cambios se realizan dentro de los 30 días de haber recibido
          dicha mercadería.
        </p>
              </div>
            </div>
          </div>
        </div>
    	</section>
      </main>
<?php

} ?>
<?php get_footer(); ?>
