<?php get_header(); ?>

<main role="main" class="container-full">


  <section id="main-image" class="bg-primary mid">
    <div class="image-cover">
      <?php echo wp_get_attachment_image(359, 'full', '', array( "class" => "img-full" ));?>
    </div>
    <div class="caption">
        <h1>
          <?php if (ICL_LANGUAGE_CODE=='es') { ?>
          FALTA ALGÚN INGREDIENTE
          <?PHP }else{ ?>
            MISSING INGREDIENT
          <?php } ?>
        </h1>
        <h3>
          <small class="text-lowercase">
            <?php if (ICL_LANGUAGE_CODE=='es') { ?>
              Página no encontrada
            <?PHP }else{ ?>
              Page not found
            <?php } ?>
          </small>
        </h3>
    </div>
  </section>

  	<div class="container py-5">
  		<div class="row">
  			<?php //get_sidebar();?>
        <section id="consultas" class="bg-white">
          <div class="container py-5">
            <div class="row">
              <div class="col-12">
                <h3 class="text-center mb-4">
                  <?php if (ICL_LANGUAGE_CODE=='es') {
                      echo "CONSULTAS";
                      } else {
                      echo "CONTACT";
                      } ?>
                </h3>
              </div>
              <div class="w100">

              </div>
              <div class="col-12 col-md-6 mapa" style="border-radius: 0 30px 0 30px; overflow: hidden; height: 300px;">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.158741358089!2d-58.53312948476968!3d-34.62542858045383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcc822029185ef%3A0x65906368a1680709!2sAv.+Gral.+Paz+7771%2C+Ciudadela%2C+Buenos+Aires!5e0!3m2!1ses-419!2sar!4v1498825133590" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
              <div class="col-12 col-md-6 txt-mid-gray pt-5">
                <p>
                  <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-3075  <br /><i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4657-5972
                   <br /> <i class="fa fa-phone" aria-hidden="true"></i> +54 (011) 4647-0539
                </p>
                <p>
                  <i class="fa fa-whatsapp" aria-hidden="true"></i> +54 (011) 155 5578 0717
                </p>
                <p>
                  <?php if (ICL_LANGUAGE_CODE=='es') {
                    echo "Vía mail a: info@hojasverdes.com.ar";
                    } else {
                    echo "info@hojasverdes.com.ar";
                    } ?>
                </p>
                <p>
                  <?php if (ICL_LANGUAGE_CODE=='es') {
                    echo "	O visite nuestro Centro de Distribución en
                      Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
                    } else {
                    echo "Avenida General Paz 7771, C.P 1702, Ciudadela Norte, Buenos Aires, Argentina";
                    } ?>

                </p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/fabrica-hojas-verdes.png" class="img-fluid" />
              </div>
            </div>
          </div>
        </section>
  		</div>
  </div>
</main>

<?php get_footer(); ?>
